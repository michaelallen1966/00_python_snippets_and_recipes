#python
#simpy
#oop


import simpy
import random
import pandas as pd
import matplotlib.pyplot as plt

class g:
    # Global variables
    inter_arrival_time=1
    los=10
    sim_duration=500
    audit_interval=1
    
class Hospital:
    def __init__(self):
        self.patients={}
        self.audit_time=[]
        self.audit_beds=[]
        self.bed_count=0
        self.admissions=0
        
    def audit(self,time):
        self.audit_time.append(time)
        self.audit_beds.append(self.bed_count)
    
    def build_audit_report(self):
        self.audit_report=pd.DataFrame()
        self.audit_report['Time']=self.audit_time
        self.audit_report['Occupied_beds']=self.audit_beds
        self.audit_report['Median_beds']=self.audit_report['Occupied_beds'].quantile(0.5)
        self.audit_report['Beds_5_percent']=self.audit_report['Occupied_beds'].quantile(0.05)
        self.audit_report['Beds_95_percent']=self.audit_report['Occupied_beds'].quantile(0.95)
        
    def chart(self):
        plt.plot(self.audit_report['Time'],self.audit_report['Occupied_beds'],color='k',marker='o',linestyle='solid',markevery=1,label='Occupied beds')
        plt.plot(self.audit_report['Time'],self.audit_report['Beds_5_percent'],color='0.5',linestyle='dashdot',markevery=1,label='5th percentile')
        plt.plot(self.audit_report['Time'],self.audit_report['Median_beds'],color='0.5',linestyle='dashed',label='Median')
        plt.plot(self.audit_report['Time'],self.audit_report['Beds_95_percent'],color='0.5',linestyle='dashdot',label='95th percentile')
        plt.xlabel('Day')
        plt.ylabel('Occupied beds') 
        plt.title('Occupied beds (individual days with 5th, 50th and 95th percentiles)')
        #plt.legend()
        plt.show()

class Model:
    def __init__(self):
        self.env=simpy.Environment()
    
    def run(self):
        self.hospital=Hospital()
        self.env.process(self.new_admission(g.inter_arrival_time,g.los))
        self.env.process(self.audit_beds(delay=20))
        self.env.run(until=g.sim_duration)
        self.hospital.build_audit_report()
        self.hospital.chart()

    def audit_beds(self,delay):
        yield self.env.timeout(delay)
        while True:
            self.hospital.audit(self.env.now)
            yield self.env.timeout(g.audit_interval)

    
    def new_admission(self,interarrival_time,los):
        while True:
            self.hospital.admissions+=1
            p=Patient(patient_id=self.hospital.admissions,
                      los=random.expovariate(1/los))
            self.hospital.patients[p.id]=p
            self.spell=self.spell_gen(p)
            self.env.process(self.spell)
            next_admission=random.expovariate(1/interarrival_time)
            # print('Next patient in %f3.2' %next_p)
            yield self.env.timeout(next_admission)
    
    def spell_gen(self,p): # patient event generator
        self.hospital.bed_count+=1
        yield self.env.timeout(p.los)
        self.hospital.bed_count-=1
        del self.hospital.patients[p.id]
        

class Patient:
    def __init__(self,patient_id,los):
        self.id=patient_id
        self.los=los
    


def main():
    model=Model()
    model.run()

if __name__=='__main__':
    main()
