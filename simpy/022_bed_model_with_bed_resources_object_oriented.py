#python
#simpy
#oop

import simpy
import random
import pandas as pd
import matplotlib.pyplot as plt

class g:
    # Global variables
    inter_arrival_time=1
    los=10
    sim_duration=500
    audit_interval=1
    beds=15
    
class Hospital:
    def __init__(self):
        self.patients={}
        self.patients_in_queue={}
        self.patients_in_beds={}
        self.audit_time=[]
        self.audit_beds=[]
        self.audit_queue=[]
        self.bed_count=0
        self.queue_count=0
        self.admissions=0

        
        
    def audit(self,time):
        self.audit_time.append(time)
        self.audit_beds.append(self.bed_count)
        self.audit_queue.append(self.queue_count)
    
    def build_audit_report(self):
        self.audit_report=pd.DataFrame()
        self.audit_report['Time']=self.audit_time
        self.audit_report['Occupied_beds']=self.audit_beds
        self.audit_report['Median_beds']=self.audit_report['Occupied_beds'].quantile(0.5)
        self.audit_report['Beds_5_percent']=self.audit_report['Occupied_beds'].quantile(0.05)
        self.audit_report['Beds_95_percent']=self.audit_report['Occupied_beds'].quantile(0.95)
        self.audit_report['Queue']=self.audit_queue
        self.audit_report['Median_queue']=self.audit_report['Queue'].quantile(0.5)
        self.audit_report['Queue_5_percent']=self.audit_report['Queue'].quantile(0.05)
        self.audit_report['Queue_95_percent']=self.audit_report['Queue'].quantile(0.95)

        
    def chart(self):
        # Plot occupied beds
        plt.plot(self.audit_report['Time'],self.audit_report['Occupied_beds'],color='k',marker='o',linestyle='solid',markevery=1,label='Occupied beds')
        plt.plot(self.audit_report['Time'],self.audit_report['Beds_5_percent'],color='0.5',linestyle='dashdot',markevery=1,label='5th percentile')
        plt.plot(self.audit_report['Time'],self.audit_report['Median_beds'],color='0.5',linestyle='dashed',label='Median')
        plt.plot(self.audit_report['Time'],self.audit_report['Beds_95_percent'],color='0.5',linestyle='dashdot',label='95th percentile')
        plt.xlabel('Day')
        plt.ylabel('Occupied beds') 
        plt.title('Occupied beds (individual days with 5th, 50th and 95th percentiles)')
        plt.legend()
        plt.show()
        
        # Plot queue for beds
        plt.plot(self.audit_report['Time'],self.audit_report['Queue'],color='k',marker='o',linestyle='solid',markevery=1,label='Occupied beds')
        plt.plot(self.audit_report['Time'],self.audit_report['Queue_5_percent'],color='0.5',linestyle='dashdot',markevery=1,label='5th percentile')
        plt.plot(self.audit_report['Time'],self.audit_report['Median_queue'],color='0.5',linestyle='dashed',label='Median')
        plt.plot(self.audit_report['Time'],self.audit_report['Queue_95_percent'],color='0.5',linestyle='dashdot',label='95th percentile')
        plt.xlabel('Day')
        plt.ylabel('Queue for beds') 
        plt.title('Queue for beds (individual days with 5th, 50th and 95th percentiles)')
        plt.legend()
        plt.show()


class Model:
    def __init__(self):
        self.env=simpy.Environment()
    
    def run(self):
        self.hospital=Hospital()
        self.resources=Resources(self.env,g.beds)
        self.env.process(self.new_admission(g.inter_arrival_time,g.los))
        self.env.process(self.audit_beds(delay=20))
        self.env.run(until=g.sim_duration)
        self.hospital.build_audit_report()
        self.hospital.chart()

    def audit_beds(self,delay):
        yield self.env.timeout(delay)
        while True:
            self.hospital.audit(self.env.now)
            yield self.env.timeout(g.audit_interval)

    
    def new_admission(self,interarrival_time,los):
        while True:
            self.hospital.admissions+=1
            p=Patient(patient_id=self.hospital.admissions,
                      los=random.expovariate(1/los))
            self.hospital.patients[p.id]=p
            self.spell=self.spell_gen(p)
            self.env.process(self.spell)
            next_admission=random.expovariate(1/interarrival_time)
            # print('Next patient in %f3.2' %next_p)
            yield self.env.timeout(next_admission)
    
    
    def spell_gen(self,p): # patient event generator
        # The following 'with' defines the required resources and automatically
        # releases resources when no longer required
        with self.resources.beds.request() as req: 
            # Increment queue count
            self.hospital.queue_count+=1
            # Add patient to dictionary of queuing patients
            # This is not used in the mode, but is an example of tracking patient objects
            self.hospital.patients_in_queue[p.id]=p
            # Yield resource request. Sim continues after yield when resources available
            yield req
            # Resource now available. Renove from queue count and dictionary of queued objects
            self.hospital.queue_count-=1
            del self.hospital.patients_in_queue[p.id]
            # Add to count of patients in beds and to dictionary of patient objects
            self.hospital.patients_in_beds[p.id]=p
            self.hospital.bed_count+=1
            # Trigger length of stay delay
            yield self.env.timeout(p.los)
            # Length of stay complete. Remove patient from counts and dictionaries
            self.hospital.bed_count-=1
            del self.hospital.patients_in_beds[p.id]
            del self.hospital.patients[p.id]
        

class Patient:
    def __init__(self,patient_id,los):
        self.id=patient_id
        self.los=los
    
class Resources:
    def __init__(self,env,number_of_beds):
        self.beds = simpy.Resource(env, capacity=number_of_beds)
        

def main():
    model=Model()
    model.run()

if __name__=='__main__':
    main()

