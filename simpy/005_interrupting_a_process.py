#python
#simpy

# The following calls an interrupt function ('driver') which, after 3 time units, interrupts the current car action.
# The interrupt is handled with try/except.

import simpy

class Car(object):
    def __init__(self,env):
        self.env=env
        # Start the run process evertime an instance is created
        self.action=env.process(self.run())

    def run(self):
        while True:
            # Parking
            print('Start parking and charging car at %d' % env.now)
            charge_duration = 5
            start_charge=env.now
            # We yield the process that process() returns
            # to wait for it to finish.
            # But this may be interrupted
            try:
                yield self.env.process(self.charge(charge_duration))
                end_charge=env.now
            except simpy.Interrupt:
                # When we received an interrupt, we stop charging and
                # start driving
                print('Was interrupted. Hope, the battery is full enough ...')
                end_charge=env.now

            # Driving starts after charge process has finished
            print('Car charged for %d time units' % (end_charge-start_charge))
            print('Start driving car at %d' % env.now)
            trip_duration=20
            yield env.timeout(trip_duration)

    def charge(self,duration):
        yield self.env.timeout(duration)

def driver(env, car):
        yield env.timeout(3)
        car.action.interrupt()


env = simpy.Environment()
car=Car(env)
env.process(driver(env, car))
env.run(until=40)

Output:

Start parking and charging car at 0
Was interrupted. Hope, the battery is full enough ...
Car charged for 3 time units
Start driving car at 3
Start parking and charging car at 23
Car charged for 5 time units
Start driving car at 28

