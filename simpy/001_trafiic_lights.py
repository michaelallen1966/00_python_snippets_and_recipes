# Simple traffic light model

import simpy

# Define the traffic light process
def traffic_light(env):
    while True: # Loop continually until simulation ends
        # Print time (by accessing env.now)
        print('Light turned green at t='+str(env.now))
        yield env.timeout(30) # delay until next command
        print('Light turned yellow at t='+str(env.now))
        yield env.timeout(5) 
        print('Light turned red at t='+str(env.now))
        yield env.timeout(15)
        print () # print an empty line
        # Function will loop back 

# Set up simpy environment
env = simpy.Environment() 

# Define the process that start when the simulation starts. There could be 
# more than one process. We always pass the simulation environment to any
# process.
env.process(traffic_light(env))

# Start simulation running (and define run time in simulation time units)
env.run(until=120)
# Simulation ends
print('Simulation complete')
