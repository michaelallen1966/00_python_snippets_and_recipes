﻿#python
#pandas
#bar chart

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

df=pd.DataFrame(np.random.rand(10,4),columns=['a','b','c','d'])
df.plot(kind='bar',grid=False)