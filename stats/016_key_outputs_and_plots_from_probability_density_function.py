﻿#scipy
#python
#stats
#seaborn

Probability Densitity Function (PDF): distribution of data

Cumulative Distribution Function (CDF): probability of obtaining a value <x

Survival Function (SF): 1-CDF. Probability of obtaining a value >x

Percentile Point Function (PPF): Given a probability, of being smaller than x what is x? e.g. at what height(x) are 95% smaller than x?

Inverse Survival Function (ISF): Given a probability, of being greater than x what is x? e.g. at what height(x) are 5% larger than x?

Random Variate Sample (RVS): a random sample from a given PDF.


import numpy as np
from scipy import stats
import seaborn
import seaborn as sns
import matplotlib.pyplot as plt


# To examine a distribution follow a two-step process
# 1) Create distribution
# 2) Examine distribution

my_distribution = stats.norm(5,3) # define frozen distribution

x = np.linspace(-5,15,101)

# Plots as function of X variable

# Cumulative Distribution Function (CDF)

y = my_distribution.cdf(x) # create cumulative distribution from distribution
plt.plot(x,y)
plt.title('Cumulative Distribution Function (CDF)')
plt.xlabel('X')
plt.ylabel('CDF')
plt.show()

# Probability Densitity Function (ODF)
y = my_distribution.pdf(x) # create cumulative distribution from distribution
plt.plot(x,y)
plt.title('Probability Distribution Function (PDF)')
plt.xlabel('X')
plt.ylabel('PDF')
plt.show()

# Survival Function (SF)
y = my_distribution.sf(x) # create cumulative distribution from distribution
plt.plot(x,y)
plt.title('Survival Function (SF)')
plt.xlabel('X')
plt.ylabel('SF')
plt.show()

# Plots as function of probability of X

x = np.linspace(0,1,101)

# Percentile Point Function (PPF)
y = my_distribution.ppf(x) # create cumulative distribution from distribution
plt.plot(x,y)
plt.title('Percentile Point Function (PPF)')
plt.xlabel('PPF')
plt.ylabel('X')
plt.show()

#Inverse Survival Function (ISF)
y = my_distribution.isf(x) # create cumulative distribution from distribution
plt.plot(x,y)
plt.title('Inverse Survival Function (ISF)')
plt.xlabel('ISF')
plt.ylabel('X')
plt.show()


