﻿#python
#stats
#cumulative frequency
#seaborn


import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Generate data that are normally distributed
x = np.random.randn(500)

numbins=10
plt.plot(stats.cumfreq(x,numbins)[0])
plt.show()