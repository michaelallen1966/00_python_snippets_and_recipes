﻿#python
#seaborn
#stats

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_context('notebook')
sns.set_style('darkgrid')


# Generate data that are normally distributed
x = np.random.randn(1000)

plt.plot(x,'.')
plt.title('Scatter Plot')
plt.xlabel('X')
plt.ylabel('Y')
plt.show()


OR

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Generate data that are normally distributed
x = np.random.randn(500)
plt.scatter(np.arange(len(x)),x)
plt.show()