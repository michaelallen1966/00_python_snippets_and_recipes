﻿#python
#stats
#seaborn
#scatter plot

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

# Bivariate Plots
df2 = pd.DataFrame(np.random.rand(50, 3), columns=['a', 'b', 'c'])
df2.plot(kind='scatter', x='a', y='b', s=df2['c']*500);
plt.axhline(0, ls='--', color='#999999')
plt.axvline(0, ls='--', color='#999999')
plt.show()