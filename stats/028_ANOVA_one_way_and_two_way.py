#python
#stats
#ANOVA

ONE WAY
=======

One way: e.g. look at effect of age group

# --- >>> START stats <<< ---
# First, check if the variances are equal, with the "Levene"-test
(W,p) = stats.levene(group1, group2, group3)
if p<0.05:
	print(('Warning: the p-value of the Levene test is <0.05: p={0}'.format(p)))
    
# Do the one-way ANOVA
F_statistic, pVal = stats.f_oneway(group1, group2, group3)


TWO WAY
=======

Two ways: e.g. look at effect of age group and sex. 
Unlike one-way ANOVA, this method identifies interactions between groups.
More than two ways may be modelled with statsmodels.
Always good to plot data first.
Seaborn has built in 'factorplot'


# Import standard packages
import numpy as np
import pandas as pd

# additional packages
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm

'''ANOVA with interaction: Measurement of fetal head circumference,
by four observers in three fetuses, from a study investigating the
reproducibility of ultrasonic fetal head circumference data.
'''
    
# Get the data
inFile = 'altman_12_6.txt' # get data from file
data = np.genfromtxt(inFile, delimiter=',')
    
# Bring them in DataFrame-format
df = pd.DataFrame(data, columns=['hs', 'fetus', 'observer'])
    
# --- >>> START stats <<< ---
# Determine the ANOVA with interaction
formula = 'hs ~ C(fetus) + C(observer) + C(fetus):C(observer)'
lm = ols(formula, df).fit()
anovaResults = anova_lm(lm)
# --- >>> STOP stats <<< ---
print(anovaResults)
