#python
#file

Note: Text files are always read as strings. Strings will need converting to numbers. There is no separation of fields with this method.
Usually Pandas or NumPy will be used rather than native Python import.


with open('Colors.txt','r') as open_file:
     print('Colors.txt content:\n'+open_file.read())

File must be in same directory as Python code.

The following amendment reads just the first 15 characters of the file.

with open('Colors.txt','r') as open_file:
     print('Colors.txt content:\n'+open_file.read(15))

Lines can be read one line at a time:

with open('Colors.txt','r') as open_file:
    for observation in open_file:
        print('Reading data: '+observation)

Sampling every n line:

n=2
with open('Colors.txt','r') as open_file:
    for j, observation in enumerate(open_file):
        if j%n==0:
            print('Reading Line: '+str(j)+'Content: '+observation)

Random sample:

from random import random
sample_size=0.25
with open('Colors.txt','r') as open_file:
    for j, observation in enumerate(open_file):
        if random()<=sample_size:
            print('Reading Line: '+str(j)+'Content: '+observation)
