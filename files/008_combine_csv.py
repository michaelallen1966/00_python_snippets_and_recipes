from glob import glob
import os

# glob searches through all files by name 

# Remove collated file if it already exists
if os.path.isfile('collated_csv.csv'):
    os.remove('collated_csv.csv')

file_count = 0
line_count = 0
with open('collated_csv.csv', 'a') as singleFile:
    for csvFile in glob('*.csv'):
        file_count += 1
        line_count = 0
        for line in open(csvFile, 'r'):
            line_count += 1
            # ignore header row of all files apart from the first file
            if file_count > 1 and line_count == 1:
                continue
            singleFile.write(line)