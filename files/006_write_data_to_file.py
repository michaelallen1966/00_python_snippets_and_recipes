# To wite (or overwrite file)
with open('z_example_saved_file.txt','w') as output_file:
    output_file.write('Computer Science\n')
    
# To add to file already present
with open('z_example_saved_file.txt','a') as output_file:
    output_file.write('Software engineering\n')    