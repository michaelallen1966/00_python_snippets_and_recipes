# Open whole file

file = open('z_example_text_file.txt','r')
# Read entire file into a string
contents = file.read()
print (contents)
# Release resources asscoaited with file
file.close()
del contents

# Using with avoids having to close file and creates a block
with open('z_example_text_file.txt','r') as file2:
    contents2 = file2.read()
print (contents2)
del contents2

# show current directory
import os
print('Current directory is:')
print (os.getcwd())

# Read part of file by length
with open('z_example_text_file.txt','r') as file3:
    # read first 10 characters
    first_ten_chars = file3.read(10)
    print(first_ten_chars)
    # read rest of file (file cursor is 10 characters in)
    the_rest = file3.read()
    print(the_rest)
del first_ten_chars
del the_rest
# To re-read a file it must be closed and re-opened

# Read contents into lines
with open('z_example_text_file.txt','r') as file4:
    lines = file4.readlines()
print(lines)
del lines

# Working with one line at a time
with open('z_example_text_file.txt','r') as file5:
    for line in file5:
        print(line)
        
# working with one line at a time, and stopping, with readline
print ('Using readline')        
with open('z_example_text_file.txt','r') as file6:
    line = ''
    while line != 'Test-stop\n':
        line = file6.readline()
        print(line)

    
    