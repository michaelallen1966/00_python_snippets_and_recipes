import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split

# Load data
data = pd.read_csv('imdb.csv')
data = data.sample(1000)

# Find length of comments
data['length'] = data['review'].str.len()
max_length = data['length'].max()
X = list(data['review'])
y = list(data['sentiment'])

# process data
vocabulary_processor = \
    tf.contrib.learn.preprocessing.VocabularyProcessor(max_length)
X_transform = list(vocabulary_processor.fit_transform(X))

# Split data
X_train,X_test,y_train,y_test = \
    train_test_split(X_transform,y,test_size=0.3,random_state=100)




