#python
#matplotlib
#bar chart


"""
Fill types:

/
\
|

-
+
x
o
O
.
*
"""


import numpy as np
import matplotlib.pyplot as plt

n=8
a=np.random.random(n)*3
b=np.random.random(n)
x=np.arange(n)
'k'
plt.bar(x,a,color='w',edgecolor='k',hatch='x')
plt.bar(x,b,bottom=a,color='w',edgecolor='k',hatch='/')

plt.show()
