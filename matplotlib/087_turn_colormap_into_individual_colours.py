#python
#matplotlib
#colormap

import matplotlib.pyplot as plt
import numpy as np

n=10
colors=[plt.cm.viridis(i) for i in np.linspace(0,1,n)]

for i in range (n):
    y=np.random.rand(4)
    plt.plot(y,
             linewidth=3,
             color=colors[i],
             label='Series '+str(i))

plt.legend()
plt.show()

