#python
#matplotlib
#arrow

# plt.annotate adds text and arrow combined.

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-4,4,1024)
y=0.25*(x+4)*(x+1)*(x-2)

box={'Facecolor':'0.90',
     'edgecolor':'k',
     'boxstyle':'square'}

plt.annotate('Cuve minimum',
             ha='center',
             va='bottom',
             xytext=(-1.5,4),
             xy=(0.75,-2.5),
             arrowprops={'facecolor':'black',
                         'shrink':0.05})

plt.title('Title')
plt.xlabel('x label')
plt.ylabel('y label')
plt.plot(x,y,c='k')

plt.show()
