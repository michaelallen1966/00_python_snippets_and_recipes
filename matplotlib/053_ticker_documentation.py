#python
#matplotlib
#tick

import matplotlib.ticker as ticker

NAME
    matplotlib.ticker

DESCRIPTION
    Tick locating and formatting
    ============================
    
    This module contains classes to support completely configurable tick
    locating and formatting.  Although the locators know nothing about major
    or minor ticks, they are used by the Axis class to support major and
    minor tick locating and formatting.  Generic tick locators and
    formatters are provided, as well as domain specific custom ones.
    
    
    Default Formatter
    -----------------
    
    The default formatter identifies when the x-data being
    plotted is a small range on top of a large off set.  To
    reduce the chances that the ticklabels overlap the ticks
    are labeled as deltas from a fixed offset.  For example::
    
       ax.plot(np.arange(2000, 2010), range(10))
    
    will have tick of 0-9 with an offset of +2e3.  If this
    is not desired turn off the use of the offset on the default
    formatter::
    
    
       ax.get_xaxis().get_major_formatter().set_useOffset(False)
    
    set the rcParam ``axes.formatter.useoffset=False`` to turn it off
    globally, or set a different formatter.
    
    Tick locating
    -------------
    
    The Locator class is the base class for all tick locators.  The locators
    handle autoscaling of the view limits based on the data limits, and the
    choosing of tick locations.  A useful semi-automatic tick locator is
    MultipleLocator.  You initialize this with a base, e.g., 10, and it
    picks axis limits and ticks that are multiples of your base.
    
    The Locator subclasses defined here are
    
    :class:`NullLocator`
        No ticks
    
    :class:`FixedLocator`
        Tick locations are fixed
    
    :class:`IndexLocator`
        locator for index plots (e.g., where x = range(len(y)))
    
    :class:`LinearLocator`
        evenly spaced ticks from min to max
    
    :class:`LogLocator`
        logarithmically ticks from min to max
    
    :class:`SymmetricalLogLocator`
        locator for use with with the symlog norm, works like the
        `LogLocator` for the part outside of the threshold and add 0 if
        inside the limits
    
    :class:`MultipleLocator`
        ticks and range are a multiple of base;
                      either integer or float
    :class:`OldAutoLocator`
        choose a MultipleLocator and dyamically reassign it for
        intelligent ticking during navigation
    
    :class:`MaxNLocator`
        finds up to a max number of ticks at nice locations
    
    :class:`AutoLocator`
        :class:`MaxNLocator` with simple defaults. This is the default
        tick locator for most plotting.
    
    :class:`AutoMinorLocator`
        locator for minor ticks when the axis is linear and the
        major ticks are uniformly spaced.  It subdivides the major
        tick interval into a specified number of minor intervals,
        defaulting to 4 or 5 depending on the major interval.
    
    :class:`LogitLocator`
        Locator for logit scaling.
    
    
    There are a number of locators specialized for date locations - see
    the dates module
    
    You can define your own locator by deriving from Locator.  You must
    override the __call__ method, which returns a sequence of locations,
    and you will probably want to override the autoscale method to set the
    view limits from the data limits.
    
    If you want to override the default locator, use one of the above or a
    custom locator and pass it to the x or y axis instance.  The relevant
    methods are::
    
      ax.xaxis.set_major_locator( xmajorLocator )
      ax.xaxis.set_minor_locator( xminorLocator )
      ax.yaxis.set_major_locator( ymajorLocator )
      ax.yaxis.set_minor_locator( yminorLocator )
    
    The default minor locator is the NullLocator, e.g., no minor ticks on by
    default.
    
    Tick formatting
    ---------------
    
    Tick formatting is controlled by classes derived from Formatter.  The
    formatter operates on a single tick value and returns a string to the
    axis.
    
    :class:`NullFormatter`
        No labels on the ticks
    
    :class:`IndexFormatter`
        Set the strings from a list of labels
    
    :class:`FixedFormatter`
        Set the strings manually for the labels
    
    :class:`FuncFormatter`
        User defined function sets the labels
    
    :class:`StrMethodFormatter`
        Use string `format` method
    
    :class:`FormatStrFormatter`
        Use an old-style sprintf format string
    
    :class:`ScalarFormatter`
        Default formatter for scalars: autopick the format string
    
    :class:`LogFormatter`
        Formatter for log axes
    
    :class:`LogFormatterExponent`
        Format values for log axis using ``exponent = log_base(value)``.
    
    :class:`LogFormatterMathtext`
        Format values for log axis using ``exponent = log_base(value)``
        using Math text.
    
    :class:`LogFormatterSciNotation`
        Format values for log axis using scientific notation.
    
    :class:`LogitFormatter`
        Probability formatter.
    
    
    You can derive your own formatter from the Formatter base class by
    simply overriding the ``__call__`` method.  The formatter class has
    access to the axis view and data limits.
    
    To control the major and minor tick label formats, use one of the
    following methods::
    
      ax.xaxis.set_major_formatter( xmajorFormatter )
      ax.xaxis.set_minor_formatter( xminorFormatter )
      ax.yaxis.set_major_formatter( ymajorFormatter )
      ax.yaxis.set_minor_formatter( yminorFormatter )
    
    See :ref:`pylab_examples-major_minor_demo1` for an example of setting
    major and minor ticks.  See the :mod:`matplotlib.dates` module for
    more information and examples of using date locators and formatters.

CLASSES
    builtins.object
        TickHelper
            Formatter
                FixedFormatter
                FormatStrFormatter
                FuncFormatter
                LogFormatter
                    LogFormatterExponent
                    LogFormatterMathtext
                NullFormatter
                ScalarFormatter
                StrMethodFormatter
            Locator
                AutoMinorLocator
                FixedLocator
                IndexLocator
                LinearLocator
                LogLocator
                MaxNLocator
                    AutoLocator
                MultipleLocator
                NullLocator
                SymmetricalLogLocator
    
    class AutoLocator(MaxNLocator)
     |  Select no more than N intervals at nice locations.
     |  
     |  Method resolution order:
     |      AutoLocator
     |      MaxNLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __init__(self)
     |      Keyword args:
     |      
     |      *nbins*
     |          Maximum number of intervals; one less than max number of
     |          ticks.  If the string `'auto'`, the number of bins will be
     |          automatically determined based on the length of the axis.
     |      
     |      *steps*
     |          Sequence of nice numbers starting with 1 and ending with 10;
     |          e.g., [1, 2, 4, 5, 10]
     |      
     |      *integer*
     |          If True, ticks will take only integer values, provided
     |          at least `min_n_ticks` integers are found within the
     |          view limits.
     |      
     |      *symmetric*
     |          If True, autoscaling will result in a range symmetric
     |          about zero.
     |      
     |      *prune*
     |          ['lower' | 'upper' | 'both' | None]
     |          Remove edge ticks -- useful for stacked or ganged plots
     |          where the upper tick of one axes overlaps with the lower
     |          tick of the axes above it, primarily when
     |          `rcParams['axes.autolimit_mode']` is `'round_numbers'`.
     |          If `prune=='lower'`, the smallest tick will
     |          be removed.  If `prune=='upper'`, the largest tick will be
     |          removed.  If `prune=='both'`, the largest and smallest ticks
     |          will be removed.  If `prune==None`, no ticks will be removed.
     |      
     |      *min_n_ticks*
     |          Relax `nbins` and `integer` constraints if necessary to
     |          obtain this minimum number of ticks.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from MaxNLocator:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  bin_boundaries(self, vmin, vmax)
     |      .. deprecated:: 2.0
     |          The bin_boundaries function was deprecated in version 2.0.
     |      
     |      \
     |  
     |  set_params(self, **kwargs)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, dmin, dmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from MaxNLocator:
     |  
     |  default_params = {'integer': False, 'min_n_ticks': 2, 'nbins': 10, 'pr...
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class AutoMinorLocator(Locator)
     |  Dynamically find minor tick positions based on the positions of
     |  major ticks. Assumes the scale is linear and major ticks are
     |  evenly spaced.
     |  
     |  Method resolution order:
     |      AutoMinorLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, n=None)
     |      *n* is the number of subdivisions of the interval between
     |      major ticks; e.g., n=2 will place a single minor tick midway
     |      between major ticks.
     |      
     |      If *n* is omitted or None, it will be set to 5 or 4.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  set_params(self, **kwargs)
     |      Do nothing, and rase a warning. Any locator class not supporting the
     |      set_params() function will call this.
     |  
     |  view_limits(self, vmin, vmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class FixedFormatter(Formatter)
     |  Return fixed strings for tick labels based only on position, not
     |  value.
     |  
     |  Method resolution order:
     |      FixedFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Returns the label that matches the position regardless of the
     |      value.
     |      
     |      For positions ``pos < len(seq)``, return `seq[i]` regardless of
     |      `x`. Otherwise return empty string. `seq` is the sequence of
     |      strings that this object was initialized with.
     |  
     |  __init__(self, seq)
     |      Set the sequence of strings that will be used for labels.
     |  
     |  get_offset(self)
     |  
     |  set_offset_string(self, ofs)
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class FixedLocator(Locator)
     |  Tick locations are fixed.  If nbins is not None,
     |  the array of possible positions will be subsampled to
     |  keep the number of ticks <= nbins +1.
     |  The subsampling will be done so as to include the smallest
     |  absolute value; for example, if zero is included in the
     |  array of possibilities, then it is guaranteed to be one of
     |  the chosen ticks.
     |  
     |  Method resolution order:
     |      FixedLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, locs, nbins=None)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  set_params(self, nbins=None)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      "
     |      Return the locations of the ticks.
     |      
     |      .. note::
     |      
     |          Because the values are fixed, vmin and vmax are not used in this
     |          method.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  view_limits(self, vmin, vmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class FormatStrFormatter(Formatter)
     |  Use an old-style ('%' operator) format string to format the tick.
     |  
     |  The format string should have a single variable format (%) in it.
     |  It will be applied to the value (not the position) of the tick.
     |  
     |  Method resolution order:
     |      FormatStrFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the formatted label string.
     |      
     |      Only the value `x` is formatted. The position is ignored.
     |  
     |  __init__(self, fmt)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  get_offset(self)
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class Formatter(TickHelper)
     |  Create a string based on a tick value and location.
     |  
     |  Method resolution order:
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the format for tick value `x` at position pos.
     |      ``pos=None`` indicates an unspecified location.
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  get_offset(self)
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class FuncFormatter(Formatter)
     |  Use a user-defined function for formatting.
     |  
     |  The function should take in two inputs (a tick value ``x`` and a
     |  position ``pos``), and return a string containing the corresponding
     |  tick label.
     |  
     |  Method resolution order:
     |      FuncFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the value of the user defined function.
     |      
     |      `x` and `pos` are passed through as-is.
     |  
     |  __init__(self, func)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  get_offset(self)
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class IndexLocator(Locator)
     |  Place a tick on every multiple of some base number of points
     |  plotted, e.g., on every 5th point.  It is assumed that you are doing
     |  index plotting; i.e., the axis is 0, len(data).  This is mainly
     |  useful for x ticks.
     |  
     |  Method resolution order:
     |      IndexLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, base, offset)
     |      place ticks on the i-th data points where (i-offset)%base==0
     |  
     |  set_params(self, base=None, offset=None)
     |      Set parameters within this locator
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  view_limits(self, vmin, vmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class LinearLocator(Locator)
     |  Determine the tick locations
     |  
     |  The first time this function is called it will try to set the
     |  number of ticks to make a nice tick partitioning.  Thereafter the
     |  number of ticks will be fixed so that interactive navigation will
     |  be nice
     |  
     |  Method resolution order:
     |      LinearLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, numticks=None, presets=None)
     |      Use presets to set locs based on lom.  A dict mapping vmin, vmax->locs
     |  
     |  set_params(self, numticks=None, presets=None)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, vmin, vmax)
     |      Try to choose the view limits intelligently
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class Locator(TickHelper)
     |  Determine the tick locations;
     |  
     |  Note, you should not use the same locator between different
     |  :class:`~matplotlib.axis.Axis` because the locator stores references to
     |  the Axis data and view limits
     |  
     |  Method resolution order:
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  set_params(self, **kwargs)
     |      Do nothing, and rase a warning. Any locator class not supporting the
     |      set_params() function will call this.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, vmin, vmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class LogFormatter(Formatter)
     |  Base class for formatting ticks on a log or symlog scale.
     |  
     |  It may be instantiated directly, or subclassed.
     |  
     |  Parameters
     |  ----------
     |  base : float, optional, default: 10.
     |      Base of the logarithm used in all calculations.
     |  
     |  labelOnlyBase : bool, optional, default: False
     |      If True, label ticks only at integer powers of base.
     |      This is normally True for major ticks and False for
     |      minor ticks.
     |  
     |  minor_thresholds : (subset, all), optional, default: (1, 0.4)
     |      If labelOnlyBase is False, these two numbers control
     |      the labeling of ticks that are not at integer powers of
     |      base; normally these are the minor ticks. The controlling
     |      parameter is the log of the axis data range.  In the typical
     |      case where base is 10 it is the number of decades spanned
     |      by the axis, so we can call it 'numdec'. If ``numdec <= all``,
     |      all minor ticks will be labeled.  If ``all < numdec <= subset``,
     |      then only a subset of minor ticks will be labeled, so as to
     |      avoid crowding. If ``numdec > subset`` then no minor ticks will
     |      be labeled.
     |  
     |  linthresh : None or float, optional, default: None
     |      If a symmetric log scale is in use, its ``linthresh``
     |      parameter must be supplied here.
     |  
     |  Notes
     |  -----
     |  The `set_locs` method must be called to enable the subsetting
     |  logic controlled by the ``minor_thresholds`` parameter.
     |  
     |  In some cases such as the colorbar, there is no distinction between
     |  major and minor ticks; the tick locations might be set manually,
     |  or by a locator that puts ticks at integer powers of base and
     |  at intermediate locations.  For this situation, disable the
     |  minor_thresholds logic by using ``minor_thresholds=(np.inf, np.inf)``,
     |  so that all ticks will be labeled.
     |  
     |  To disable labeling of minor ticks when 'labelOnlyBase' is False,
     |  use ``minor_thresholds=(0, 0)``.  This is the default for the
     |  "classic" style.
     |  
     |  Examples
     |  --------
     |  To label a subset of minor ticks when the view limits span up
     |  to 2 decades, and all of the ticks when zoomed in to 0.5 decades
     |  or less, use ``minor_thresholds=(2, 0.5)``.
     |  
     |  To label all minor ticks when the view limits span up to 1.5
     |  decades, use ``minor_thresholds=(1.5, 1.5)``.
     |  
     |  Method resolution order:
     |      LogFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the format for tick val `x`.
     |  
     |  __init__(self, base=10.0, labelOnlyBase=False, minor_thresholds=None, linthresh=None)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  base(self, base)
     |      change the `base` for labeling.
     |      
     |      .. warning::
     |         Should always match the base used for :class:`LogLocator`
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short formatted string representation of a number.
     |  
     |  label_minor(self, labelOnlyBase)
     |      Switch minor tick labeling on or off.
     |      
     |      Parameters
     |      ----------
     |      labelOnlyBase : bool
     |          If True, label ticks only at integer powers of base.
     |  
     |  pprint_val(self, x, d)
     |  
     |  set_locs(self, locs=None)
     |      Use axis view limits to control which ticks are labeled.
     |      
     |      The ``locs`` parameter is ignored in the present algorithm.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  get_offset(self)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class LogFormatterExponent(LogFormatter)
     |  Format values for log axis using ``exponent = log_base(value)``.
     |  
     |  Method resolution order:
     |      LogFormatterExponent
     |      LogFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods inherited from LogFormatter:
     |  
     |  __call__(self, x, pos=None)
     |      Return the format for tick val `x`.
     |  
     |  __init__(self, base=10.0, labelOnlyBase=False, minor_thresholds=None, linthresh=None)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  base(self, base)
     |      change the `base` for labeling.
     |      
     |      .. warning::
     |         Should always match the base used for :class:`LogLocator`
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short formatted string representation of a number.
     |  
     |  label_minor(self, labelOnlyBase)
     |      Switch minor tick labeling on or off.
     |      
     |      Parameters
     |      ----------
     |      labelOnlyBase : bool
     |          If True, label ticks only at integer powers of base.
     |  
     |  pprint_val(self, x, d)
     |  
     |  set_locs(self, locs=None)
     |      Use axis view limits to control which ticks are labeled.
     |      
     |      The ``locs`` parameter is ignored in the present algorithm.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  get_offset(self)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class LogFormatterMathtext(LogFormatter)
     |  Format values for log axis using ``exponent = log_base(value)``.
     |  
     |  Method resolution order:
     |      LogFormatterMathtext
     |      LogFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the format for tick value `x`.
     |      
     |      The position `pos` is ignored.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from LogFormatter:
     |  
     |  __init__(self, base=10.0, labelOnlyBase=False, minor_thresholds=None, linthresh=None)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  base(self, base)
     |      change the `base` for labeling.
     |      
     |      .. warning::
     |         Should always match the base used for :class:`LogLocator`
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short formatted string representation of a number.
     |  
     |  label_minor(self, labelOnlyBase)
     |      Switch minor tick labeling on or off.
     |      
     |      Parameters
     |      ----------
     |      labelOnlyBase : bool
     |          If True, label ticks only at integer powers of base.
     |  
     |  pprint_val(self, x, d)
     |  
     |  set_locs(self, locs=None)
     |      Use axis view limits to control which ticks are labeled.
     |      
     |      The ``locs`` parameter is ignored in the present algorithm.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  get_offset(self)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class LogLocator(Locator)
     |  Determine the tick locations for log axes
     |  
     |  Method resolution order:
     |      LogLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, base=10.0, subs=(1.0,), numdecs=4, numticks=None)
     |      Place ticks on the locations : subs[j] * base**i
     |      
     |      Parameters
     |      ----------
     |      subs : None, string, or sequence of float, optional, default (1.0,)
     |          Gives the multiples of integer powers of the base at which
     |          to place ticks.  The default places ticks only at
     |          integer powers of the base.
     |          The permitted string values are ``'auto'`` and ``'all'``,
     |          both of which use an algorithm based on the axis view
     |          limits to determine whether and how to put ticks between
     |          integer powers of the base.  With ``'auto'``, ticks are
     |          placed only between integer powers; with ``'all'``, the
     |          integer powers are included.  A value of None is
     |          equivalent to ``'auto'``.
     |  
     |  base(self, base)
     |      set the base of the log scaling (major tick every base**i, i integer)
     |  
     |  nonsingular(self, vmin, vmax)
     |  
     |  set_params(self, base=None, subs=None, numdecs=None, numticks=None)
     |      Set parameters within this locator.
     |  
     |  subs(self, subs)
     |      set the minor ticks for the log scaling every base**i*subs[j]
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, vmin, vmax)
     |      Try to choose the view limits intelligently
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class MaxNLocator(Locator)
     |  Select no more than N intervals at nice locations.
     |  
     |  Method resolution order:
     |      MaxNLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, *args, **kwargs)
     |      Keyword args:
     |      
     |      *nbins*
     |          Maximum number of intervals; one less than max number of
     |          ticks.  If the string `'auto'`, the number of bins will be
     |          automatically determined based on the length of the axis.
     |      
     |      *steps*
     |          Sequence of nice numbers starting with 1 and ending with 10;
     |          e.g., [1, 2, 4, 5, 10]
     |      
     |      *integer*
     |          If True, ticks will take only integer values, provided
     |          at least `min_n_ticks` integers are found within the
     |          view limits.
     |      
     |      *symmetric*
     |          If True, autoscaling will result in a range symmetric
     |          about zero.
     |      
     |      *prune*
     |          ['lower' | 'upper' | 'both' | None]
     |          Remove edge ticks -- useful for stacked or ganged plots
     |          where the upper tick of one axes overlaps with the lower
     |          tick of the axes above it, primarily when
     |          `rcParams['axes.autolimit_mode']` is `'round_numbers'`.
     |          If `prune=='lower'`, the smallest tick will
     |          be removed.  If `prune=='upper'`, the largest tick will be
     |          removed.  If `prune=='both'`, the largest and smallest ticks
     |          will be removed.  If `prune==None`, no ticks will be removed.
     |      
     |      *min_n_ticks*
     |          Relax `nbins` and `integer` constraints if necessary to
     |          obtain this minimum number of ticks.
     |  
     |  bin_boundaries(self, vmin, vmax)
     |      .. deprecated:: 2.0
     |          The bin_boundaries function was deprecated in version 2.0.
     |      
     |      \
     |  
     |  set_params(self, **kwargs)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, dmin, dmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  default_params = {'integer': False, 'min_n_ticks': 2, 'nbins': 10, 'pr...
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class MultipleLocator(Locator)
     |  Set a tick on every integer that is multiple of base in the
     |  view interval
     |  
     |  Method resolution order:
     |      MultipleLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, base=1.0)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  set_params(self, base)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, dmin, dmax)
     |      Set the view limits to the nearest multiples of base that
     |      contain the data
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class NullFormatter(Formatter)
     |  Always return the empty string.
     |  
     |  Method resolution order:
     |      NullFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Returns an empty string for all inputs.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  get_offset(self)
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class NullLocator(Locator)
     |  No ticks
     |  
     |  Method resolution order:
     |      NullLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  tick_values(self, vmin, vmax)
     |      "
     |      Return the locations of the ticks.
     |      
     |      .. note::
     |      
     |          Because the values are Null, vmin and vmax are not used in this
     |          method.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  set_params(self, **kwargs)
     |      Do nothing, and rase a warning. Any locator class not supporting the
     |      set_params() function will call this.
     |  
     |  view_limits(self, vmin, vmax)
     |      select a scale for the range from vmin to vmax
     |      
     |      Normally this method is overridden by subclasses to
     |      change locator behaviour.
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class ScalarFormatter(Formatter)
     |  Format tick values as a number.
     |  
     |  Tick value is interpreted as a plain old number. If
     |  ``useOffset==True`` and the data range is much smaller than the data
     |  average, then an offset will be determined such that the tick labels
     |  are meaningful. Scientific notation is used for ``data < 10^-n`` or
     |  ``data >= 10^m``, where ``n`` and ``m`` are the power limits set
     |  using ``set_powerlimits((n,m))``. The defaults for these are
     |  controlled by the ``axes.formatter.limits`` rc parameter.
     |  
     |  Method resolution order:
     |      ScalarFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the format for tick value `x` at position `pos`.
     |  
     |  __init__(self, useOffset=None, useMathText=None, useLocale=None)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  fix_minus(self, s)
     |      Replace hyphens with a unicode minus.
     |  
     |  format_data(self, value)
     |      Return a formatted string representation of a number.
     |  
     |  format_data_short(self, value)
     |      Return a short formatted string representation of a number.
     |  
     |  get_offset(self)
     |      Return scientific notation, plus offset.
     |  
     |  get_useLocale(self)
     |  
     |  get_useOffset(self)
     |  
     |  pprint_val(self, x)
     |  
     |  set_locs(self, locs)
     |      Set the locations of the ticks.
     |  
     |  set_powerlimits(self, lims)
     |      Sets size thresholds for scientific notation.
     |      
     |      ``lims`` is a two-element sequence containing the powers of 10
     |      that determine the switchover threshold. Numbers below
     |      ``10**lims[0]`` and above ``10**lims[1]`` will be displayed in
     |      scientific notation.
     |      
     |      For example, ``formatter.set_powerlimits((-3, 4))`` sets the
     |      pre-2007 default in which scientific notation is used for
     |      numbers less than 1e-3 or greater than 1e4.
     |      
     |      .. seealso:: Method :meth:`set_scientific`
     |  
     |  set_scientific(self, b)
     |      Turn scientific notation on or off.
     |      
     |      .. seealso:: Method :meth:`set_powerlimits`
     |  
     |  set_useLocale(self, val)
     |  
     |  set_useOffset(self, val)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  useLocale
     |  
     |  useOffset
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class StrMethodFormatter(Formatter)
     |  Use a new-style format string (as used by `str.format()`)
     |  to format the tick.
     |  
     |  The field used for the value must be labeled `x` and the field used
     |  for the position must be labeled `pos`.
     |  
     |  Method resolution order:
     |      StrMethodFormatter
     |      Formatter
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self, x, pos=None)
     |      Return the formatted label string.
     |      
     |      `x` and `pos` are passed to `str.format` as keyword arguments
     |      with those exact names.
     |  
     |  __init__(self, fmt)
     |      Initialize self.  See help(type(self)) for accurate signature.
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Formatter:
     |  
     |  fix_minus(self, s)
     |      Some classes may want to replace a hyphen for minus with the
     |      proper unicode symbol (U+2212) for typographical correctness.
     |      The default is to not replace it.
     |      
     |      Note, if you use this method, e.g., in :meth:`format_data` or
     |      call, you probably don't want to use it for
     |      :meth:`format_data_short` since the toolbar uses this for
     |      interactive coord reporting and I doubt we can expect GUIs
     |      across platforms will handle the unicode correctly.  So for
     |      now the classes that override :meth:`fix_minus` should have an
     |      explicit :meth:`format_data_short` method
     |  
     |  format_data(self, value)
     |      Returns the full string representation of the value with the
     |      position unspecified.
     |  
     |  format_data_short(self, value)
     |      Return a short string version of the tick value.
     |      
     |      Defaults to the position-independent long value.
     |  
     |  get_offset(self)
     |  
     |  set_locs(self, locs)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Formatter:
     |  
     |  locs = []
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class SymmetricalLogLocator(Locator)
     |  Determine the tick locations for symmetric log axes
     |  
     |  Method resolution order:
     |      SymmetricalLogLocator
     |      Locator
     |      TickHelper
     |      builtins.object
     |  
     |  Methods defined here:
     |  
     |  __call__(self)
     |      Return the locations of the ticks
     |  
     |  __init__(self, transform=None, subs=None, linthresh=None, base=None)
     |      place ticks on the location= base**i*subs[j]
     |  
     |  set_params(self, subs=None, numticks=None)
     |      Set parameters within this locator.
     |  
     |  tick_values(self, vmin, vmax)
     |      Return the values of the located ticks given **vmin** and **vmax**.
     |      
     |      .. note::
     |          To get tick locations with the vmin and vmax values defined
     |          automatically for the associated :attr:`axis` simply call
     |          the Locator instance::
     |      
     |              >>> print((type(loc)))
     |              <type 'Locator'>
     |              >>> print((loc()))
     |              [1, 2, 3, 4]
     |  
     |  view_limits(self, vmin, vmax)
     |      Try to choose the view limits intelligently
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from Locator:
     |  
     |  autoscale(self)
     |      autoscale the view limits
     |  
     |  pan(self, numsteps)
     |      Pan numticks (can be positive or negative)
     |  
     |  raise_if_exceeds(self, locs)
     |      raise a RuntimeError if Locator attempts to create more than
     |      MAXTICKS locs
     |  
     |  refresh(self)
     |      refresh internal information based on current lim
     |  
     |  zoom(self, direction)
     |      Zoom in/out on axis; if direction is >0 zoom in, else zoom out
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from Locator:
     |  
     |  MAXTICKS = 1000
     |  
     |  ----------------------------------------------------------------------
     |  Methods inherited from TickHelper:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors inherited from TickHelper:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes inherited from TickHelper:
     |  
     |  axis = None
    
    class TickHelper(builtins.object)
     |  Methods defined here:
     |  
     |  create_dummy_axis(self, **kwargs)
     |  
     |  set_axis(self, axis)
     |  
     |  set_bounds(self, vmin, vmax)
     |  
     |  set_data_interval(self, vmin, vmax)
     |  
     |  set_view_interval(self, vmin, vmax)
     |  
     |  ----------------------------------------------------------------------
     |  Data descriptors defined here:
     |  
     |  __dict__
     |      dictionary for instance variables (if defined)
     |  
     |  __weakref__
     |      list of weak references to the object (if defined)
     |  
     |  ----------------------------------------------------------------------
     |  Data and other attributes defined here:
     |  
     |  axis = None

DATA
    __all__ = ('TickHelper', 'Formatter', 'FixedFormatter', 'NullFormatter...

FILE
    c:\users\micha\anaconda3\lib\site-packages\matplotlib\ticker.py


