#python
#matplotlib
#contour
#mandelbrot
#array

# note: contourf replaces countour

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# Mandlebrot function
def iter_count(c,max_iter):
    x=c
    for n in range(max_iter):
        if abs(x)>2:
            return (n)
        x=x**2+c
    return (max_iter)

n=512
max_iter=64
xmin,xmax,ymin,ymax = -0.32,-0.22,0.8,0.9
x_series=np.linspace(xmin,xmax,n)
y_series=np.linspace(ymin,ymax,n)
z=np.empty((n,n))

for i,y in enumerate(y_series):
    for j,x in enumerate(x_series):
        z[i,j]=iter_count(complex(x,y),max_iter)

levels=[0,8,12,16,20,24,32]
ct=plt.contourf(x_series,y_series,z,levels,cmap=cm.gray,antialiased=True) # attaches labels

plt.show()
