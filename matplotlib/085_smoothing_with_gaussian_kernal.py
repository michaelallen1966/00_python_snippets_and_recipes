#python
#matplotlib
#smoothing
#gaussian
#kernal

This example woks in Jupyter Notebooks only
See https://en.wikipedia.org/wiki/Kernel_density_estimation

import numpy as np
from scipy.stats import gaussian_kde
from ipywidgets import interact
import matplotlib.pyplot as plt

samples = np.random.normal(0,5,100) # mean=0, sigma=5, n=100
xs = np.linspace(-10,10,100)

def plot_kde (smoothing = (0.01,1.0,0.02)):
    plt.clf()
    density = gaussian_kde(samples,smoothing)
    plt.plot(xs,density(xs))
    plt.show()

i=interact(plot_kde)


