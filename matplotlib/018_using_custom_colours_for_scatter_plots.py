#python
#matplotlib
#scatter plot

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm # colormaps

n=256
angle=np.linspace(0,8*2*np.pi,n)
radius=np.linspace(0.5,1,n)

x=radius*np.cos(angle)
y=radius*np.sin(angle)

plt.scatter(x,y,c=angle,cmap=cm.hsv)
# color is taken from the angle variable
# the colormap hsv is used (full spectrum)

plt.show()

