#python
#matplotlib
#3D
#surface

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

x=np.linspace(-3,3,256)
y=np.linspace(-3,3,256)
X,Y=np.meshgrid(x,y)
Z=np.sinc(np.sqrt(X**2 + Y**2))

fig=plt.figure()
ax=fig.gca(projection='3d')
ax.plot_surface(X,Y,Z,cmap=cm.gray)

plt.show()


