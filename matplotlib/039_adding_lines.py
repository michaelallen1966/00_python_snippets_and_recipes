#python
#matplotlib

import matplotlib.pyplot as plt

n=16

for i in range(n):
    plt.gca().add_line(plt.Line2D((0,i),(n-i,0),c='r',ls='-.'))

plt.grid(True)
plt.axis('scaled')
    
plt.show()
