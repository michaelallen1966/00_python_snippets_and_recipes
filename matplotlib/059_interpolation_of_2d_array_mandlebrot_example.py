#python
#matplotlib
#array
#mandlebrot
#interpolation


# When data definition (array size) is lower than plot array size there are a number of interpolation methods. 

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def iter_count(c,max_iter):
    x=c
    for n in range(max_iter):
        if abs(x)>2:
            return (n)
        x=x**2+c
    return (max_iter)

n=32
max_iter=64
xmin,xmax,ymin,ymax=-2.2,0.8,-1.5,1.5

x_series=np.linspace(xmin,xmax,n)
y_series=np.linspace(ymin,ymax,n)
z=np.empty((n,n))

for i,y in enumerate(y_series):
    for j,x in enumerate(x_series):
        z[i,j]=iter_count(complex(x,y),max_iter)

plt.imshow(z,cmap=cm.binary,interpolation='nearest',extent=(xmin,xmax,ymin,ymax))
plt.show()


# Try other examples:
#
# 'nearest' interpolation
# 'bilinear'
# 'bicubic'
