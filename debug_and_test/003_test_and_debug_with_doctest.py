def my_test_function(a,b):
    """ (float, float) -> float

    >>> my_test_function (10.0, 9.0)
    19.0

    >>> my_test_function (4.5, 4.1)
    8.6
    
    To use automated test use:
        import.doctest
        doctest.testmod()
        
    This won't display anything unless an example fails, in which case the
    failing example(s) and the cause(s) of the failure(s) are printed to stdout
    (why not stderr? because stderr is a lame hack <0.2 wink>), and the final
    line of output is "Test failed.".
    
    Run it with the -v switch instead:
    
    python M.py -v
    
    and a detailed report of all examples tried is printed to stdout, along
    with assorted summaries at the end.
    
    You can force verbose mode by passing "verbose=True" to testmod, or prohibit
    it by passing "verbose=False".  In either of those cases, sys.argv is not
        examined by testmod.

    
    """


    result = a + b

    return result

def _test():
    import doctest
    doctest.testmod(verbose=True)
    
if __name__ == "__main__":
    _test()
