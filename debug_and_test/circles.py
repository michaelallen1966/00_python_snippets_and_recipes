from math import pi

def calculate_araea_of_circle(radius):
    
    # Check for negative values
    if radius < 0:
        raise ValueError('The radius cannot be negative')
    
    # Check for type of input
    if type(radius) not in [int, float]:
        raise TypeError('The input must be a non-neagtive real number')
    
    area = pi * radius ** 2

    return area


if __name__ == '__main__':
    # Manually try different soucres of error, e/g/ use negative number, 
    # or a string, or a Boolean
    
    print (calculate_araea_of_circle(True))