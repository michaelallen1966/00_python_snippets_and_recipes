import unittest
from math import pi
from circles import calculate_araea_of_circle

"""To run the test from shell type
`python -m unittest test_circles`, or
`python -m unittest` (locates and rruns all test functions)
-m tells python to run as a script

Or use `unittest.main()` in this method
"""


class TestCircleArea(unittest.TestCase):
    # Each test method must start with 'test'
    
    def test_area(self):
        """ 
        Test areas when radius > 0.
        AssertAlmostEqual tests to 7 sig figures
        pass test and expected value to method
        """
        
        # Test radius = 1
        test = calculate_araea_of_circle(1)
        expected = pi
        self.assertAlmostEqual(test, expected)
        
        # Test radius = 0
        test = calculate_araea_of_circle(0)
        expected = 0
        self.assertAlmostEqual(test, expected)
        
        # Test radius = 2
        test = calculate_araea_of_circle(2)
        expected = pi * 2**2
        self.assertAlmostEqual(test, expected)
        
        
    def test_values(self):
        """Makes sure value errors are raised as necessary"""
        
        # -2 should raise a ValueError
        self.assertRaises(ValueError, calculate_araea_of_circle, -2)
        
    
    def test_types(self):
        """Check a type error is raised the the input is not a real number
        """

        # Check for complex number
        self.assertRaises(TypeError, calculate_araea_of_circle, 3j)
        
        # Check for Boolean
        self.assertRaises(TypeError, calculate_araea_of_circle, True)
        
        # Check for stringe
        self.assertRaises(TypeError, calculate_araea_of_circle, "A string")


if __name__ == '__main__':
    unittest.main() # executes all methods that beign with test
        