import unittest

"""Note: Test methods may be saved as a separate file and imported
"""

class Temperature():
    
    def convert_to_celsius(fahrenheit):
        """ (number) -> float
    
        Return the number of Celsius degrees equivalent to fahrenheit degrees.
    
        >>> convert_to_celsius(75)
        23.88888888888889
        """
                            
        return (fahrenheit - 32.0) * 5.0 / 9.0
    
    def above_freezing(celsius):
        """ (number) -> bool
    
        Return True iff temperature celsius degrees is above freezing.
    
        >>> above_freezing(5.2)
        True
        >>> above_freezing(-2)
        False
        """
        
        return celsius > 0


class TestAboveFreezing(unittest.TestCase):
    """Tests for temperature.above_freezing.
    assertEqual comes from unittest.
    Each test should be in its own method"""

    def test_above_freezing_above(self):
        """Test a temperature that is above freezing."""

        expected = True
        actual = Temperature.above_freezing(5.2)
        self.assertEqual(expected, actual,
            "The temperature is above freezing.") # display message if fail

    def test_above_freezing_below(self):
        """Test a temperature that is below freezing."""

        expected = False
        actual = Temperature.above_freezing(-2)
        self.assertEqual(expected, actual,
            "The temperature is below freezing.")

    def test_above_freezing_at_zero(self):
        """Test a temperature that is at freezing."""

        expected = False
        actual = Temperature.above_freezing(0)
        self.assertEqual(expected, actual,
            "The temperature is at the freezing mark.")

unittest.main() # executes all methods that beign with test
