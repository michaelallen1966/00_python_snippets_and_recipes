import gym

"""
Balancing a stick on a platform.
Observation reports 
1) x co-ord of centre of mass
2) sticks speed
3) sticks angle to the platform
4) sticks angular velocity

Reward of 1 for every time step stick ha snot fallen over
"""


env = gym.make("CartPole-v0")

total_reward = 0.0
total_steps = 0
obs = env.reset()

while True:
    action = env.action_space.sample() # randion action sample
    obs, reward, done, _ = env.step(action)
    print (obs)
    total_reward += reward
    total_steps += 1
    if done:
        break

print("Episode done in %d steps, total reward %.2f" % (
        total_steps, total_reward))


# Can do this manually by reseeting environment and using 
# env.step (0) for left movement or env.step(1) for right 
# movement