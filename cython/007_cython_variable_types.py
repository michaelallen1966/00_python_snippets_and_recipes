All c variable types may be used, e.g.

numbers                             cdef int
                                    cdef long
                                    cdef long long
                                    cdef double
                                    cdef double double
                                    cdef unsigned int
                                    cdef unsigned long
                                    
                                    
strings                             cdef char* s = python_s


pointers                            cdef int *p
                                    cdef void **buf

stack allocated C arrays            cdef int arr[10]
                                    cdef double points[10][20]

typedef alias types                 cdef size_t len

compound types (structs & unions)   
                                    cdef struct Grail:
                                        int age
                                        float volume

                                    cdef union Food:
                                        char *spam
                                        float *eggs

                                    cdef enum CheeseType:
                                        cheddar, edam,
                                        camembert

                                    cdef enum CheeseState:
                                        hard = 1
                                        soft = 2
                                        runny = 3



function pointers                   cdef void (*f)(int,double)




