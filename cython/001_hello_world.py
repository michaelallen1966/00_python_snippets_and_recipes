1) Create python.pyx file
---------------------

Create a 'hello world' python file
Rename .py file to .pyx


2) Create a setup.py file
-------------------------

from distutils.core import setup
from Cython.Build import cythonize

setup(ext_modules = cythonize("helloworld.pyx"))


3) Run the setup file
---------------------

python setup.py build_ext --inplace

This create a .so file in Linux or a .pyd file in Windows
This may be imported (without extension) as a normal module

>>> import helloworld
