#python
#string

see also https://docs.python.org/3.4/library/stdtypes.html#str.capitalize

Use end to replace default new-line with another character (or no character) to end string. 
print ("Hello World!", end=" ")

Use comma to print multiple items. This adds a space between each one. Use + for no space
print ("Hello",NameVar)
print ("Hello "+NameVar)

Escape characters
"\n" adds a carriage return mid string.
"\t" is tab
"\\" prints one backslash
"\"" prints a quote mark

Print a string multiple times:
print ("hello "*10)

Use / in strings where a special character would cause problems, e.g. 'There\'s a snake in my boot'

"MONTY"[4] returns the fifth letter of MONTY

Some string functions

len()
upper() 
lower()
str()
format() (e.g. to treat string elements as numbers)
join()
split() (e.g. use to separate comma separated text)
replace()
startswith(), endswith()
ord() (return character number)
int() or float() converts string to number


concatenation: print ("spam "+ "eggs"). Need to use str() to concatenate a value with a string

%s used to combine string and variables:
     name="mike"
     weather="rain"
     print ("Hello %s. Today's weather is %s" % (name,weather))


Input strings:
name="raw input"("What is your name")?

string.isalpha() tests whether s string is all alphabetical

-------------------------------------------------------------------------------------------------------

"""Some String Functions"""


S=Spam
line=spam,egg,chips,beans
hello= hello world


# Find. Locates start position of searcg string or -1 if not found
print(S.find(pa))
print(S.find(xx))


# Replace
print(S.replace(pa,XYZ))
print(S.replace(xx,XYZ)) # returns unchanged string


# Convert to upper or lower case
print(S.upper())
print(S.lower())


# Parse (split)
print(line.split(,))


# Content test
print(S.isalpha())
print(S.isdigit()) # Other tests exist


# Removing leading or trailing white space
print(hello.rstrip())
print(hello.lstrip())


# Convert to number
a=1.23
print(float(a))


a=2
print(int(a)) # errors if a is not an integer


# Length of string
print(len(S))


List of all string methods (obtained using dir(S) after S defined as a string):

capitalize
casefold
center
count
encode
endswith
expandtabs
find
format
format_map
index
isalnum
isalpha
isdecimal
isdigit
isidentifier
islower
isnumeric
isprintable
isspace
istitle
isupper
join
ljust
lower
lstrip
maketrans
partition
replace
rfind
rindex
rjust
rpartition
rsplit
rstrip
split
splitlines
startswith
strip
swapcase
title
translate
upper
zfill


