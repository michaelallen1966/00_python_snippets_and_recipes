#python
#machine learning

### Sigmoid plot ####

import numpy as np
import matplotlib.pyplot as plt

def sigmoid(z):
    return 1.0 / (1.0 + np.exp(-z))
    
z=np.arange(-7,7,0.1)
phi_z=sigmoid(z)

plt.plot(z,phi_z)
plt.axvline(0.0,color='k') # adds vertical line at x=0
plt.axhspan(0.0,1.0,facecolor='1.0',alpha=1.0,ls='dotted') # ?
plt.axhline(y=0.5,ls='dotted',color='k') # adds a dotted line at y=0.5
plt.yticks([0.0,0.5,1.0]) # changes tickmarks to given values (previously auto at 0.2)
plt.ylim(-0.1,1.1) # adds 0.1 space either side of y=0to1
plt.ylabel('$\phi(z)$') # display as symbols/equation
plt.show()


