#python
#sklearn
#machine learning
#support vector machines
#svm
#rbf
#kernel 
#matplotlib

### Note that this is the same as the non-linear max margin SVM apart from kernel is changed to rbf
# Uses a XOR data set

import numpy as np
import matplotlib.pyplot as plt

# Load data
#from sklearn import datasets
#iris=datasets.load_iris()
#X=iris.data[:,[2,3]]
#y=iris.target

np.random.seed(0)
X_xor=np.random.randn(200,2) # Standard normal distribution
y_xor=np.logical_xor(X_xor[:,0]>0,X_xor[:,1]>0)
y_xor=np.where(y_xor,1,-1)

plt.scatter(X_xor[y_xor==1,0],X_xor[y_xor==1,1],c='b',marker='x',label='1')
plt.scatter(X_xor[y_xor==-1,0],X_xor[y_xor==-1,1],c='r',marker='s',label='-1')
plt.ylim(-3.0)
plt.legend()
plt.show()

# Split data set
#from sklearn.cross_validation import train_test_split
#X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)

# Standardise data based on mean and stdev
#from sklearn.preprocessing import StandardScaler
#sc=StandardScaler() # new Standard Scalar object
#sc.fit(X_train)
#X_train_std=sc.transform(X_train)
#X_test_std=sc.transform(X_test)

# Run model
from sklearn.svm import SVC
svm=SVC(kernel='rbf',C=10.0,random_state=0,gamma=0.1) # High gamma leads to more precision, but over-fitting
svm.fit(X_xor,y_xor)

# Test
#y_pred=svm.predict(X_test_std)
#print('Misclassified samples: %d' %(y_test !=y_pred).sum())

# Or use sklearn metrics
#from sklearn.metrics import accuracy_score
#print('Accuracy: %.2f' %accuracy_score(y_test,y_pred))

# Plot
from matplotlib.colors import ListedColormap
import matplotlib.pyplot as plt
def plot_decision_regions(X,y,classifier,test_idx=None,resolution=0.02):
    # Set up marker generator and colour map
    markers=('s','x','o','^','v')
    colors=('red','blue','lightgreen','gray','cyan')
    cmap=ListedColormap(colors[:len(np.unique(y))])
    # Plot the decison surface
    x1_min,x1_max=X[:,0].min()-1,X[:,0].max()+1
    x2_min,x2_max=X[:,1].min()-1,X[:,1].max()+1
    xx1,xx2=np.meshgrid(np.arange(x1_min,x1_max,resolution),np.arange(x2_min,x2_max,resolution))
    Z=classifier.predict(np.array([xx1.ravel(),xx2.ravel()]).T)
    Z=Z.reshape(xx1.shape)
    plt.contourf(xx1,xx2,Z,alpha=0.4,cmap=cmap)
    plt.xlim(xx1.min(),xx1.max())
    plt.ylim(xx2.min(),xx2.max())
    # plot all samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y==cl,0],y=X[y==cl,1],alpha=0.8,c=cmap(idx),marker=markers[idx],label=cl)
        # Highlight test samples
    if test_idx:
        X_test,y_test=X[test_idx,:],y[test_idx]
        plt.scatter(X_test[:,0],X_test[:,1],c='',alpha=1.0,linewidths=1,marker='o',s=55,label='test set')

#X_combined_std=np.vstack((X_train_std,X_test_std))
#y_combined=np.hstack((y_train,y_test))

plot_decision_regions(X=X_xor,y=y_xor,classifier=svm)
#plt.xlabel('petal length [standardised]')
#plt.ylabel('petal width [standardised]')
plt.legend(loc='upper left')
plt.show()

