#python
#pygame

Draw methods:
    * rect      Rectangle
    * polygon   Polygone with 3+ sides
    * circle
    * ellipse
    * arc
    * line
    * lines
    * aaline    Anti-aliased (smooth) line
    * aalines
    

The return function is always a Rect object that gives the area of the screen to draw to.    
    
rect
----

Final mumber is width. If omited then rectangle is filled.

pygame.draw.rect(screen, random_color, Rect(random_pos, random_size),1)

The following code draws random rectangles on the screen

    import pygame
    from pygame.locals import *
    from sys import exit 

    from random import *

    pygame.init()
    screen = pygame.display.set_mode((640, 480), 0, 32)

    screen.lock()
    for count in range(10):
        random_color = (randint(0,255), randint(0,255), randint(0,255))
        random_pos = (randint(0,639), randint(0,479))
        random_size = (639-randint(random_pos[0],639), 479-randint(random_pos[1],479))
        pygame.draw.rect(screen, random_color, Rect(random_pos, random_size),5)

    screen.unlock()

    pygame.display.update()

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                exit()
            
polygon
-------

Takes any series of co-ordinates to draw a polygon (list of tuples).


circle
------

Takes surface, colour, centre, radius, width
Omit width or set to zero to fill

    import pygame
    from pygame.limport pygame
    from pygame.locals import *
    from sys import exit
     
    from random import *

    pygame.init()
    screen = pygame.display.set_mode((640, 480), 0, 32)


    for _ in range(25):
        random_color  = (randint(0,255), randint(0,255), randint(0,255))
        random_pos    = (randint(0,639), randint(0,479))
        random_radius = randint(1,200)
        width = 1 # set to zero to fill
        pygame.draw.circle(screen, random_color, random_pos, random_radius, width)

        pygame.display.update()


    while True:

       for event in pygame.event.get():
          if event.type == QUIT:
             pygame.quit()
             exit()ocals import *
    from sys import exit
     
    from random import *

    pygame.init()
    screen = pygame.display.set_mode((640, 480), 0, 32)


    for _ in range(25):
        random_color  = (randint(0,255), randint(0,255), randint(0,255))
        random_pos    = (randint(0,639), randint(0,479))
        random_radius = randint(1,200)
        width = 1 # set to zero to fill
        pygame.draw.circle(screen, random_color, random_pos, random_radius, width)

        pygame.display.update()


    while True:

       for event in pygame.event.get():
          if event.type == QUIT:
             pygame.quit()
             exit()

 
