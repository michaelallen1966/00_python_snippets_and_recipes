#python
#pygame

Creating surfaces
-----------------

Surface are containers that contain images that can be copied to memory.
The screen is a type of surface

pygame.image.load(image_name) # loads image to surface

pygane.display.set_mode(screen_size,flags,colour_depth) # creates a screen surface

blank_suface=pygane.Surface((256,256),depth=32) # creates a blank surface.
# depth is optional (if missing or 0 will match screen, but depth=32 allows for alpha channel)


Converting surfaces
-------------------

It is best to convert images to the same kind as the screen to ensure things run faster:

pygame.image.load(image_name).convert()

When an alpha channel is present use:

pygame.image.load(image_name).convert_alpha()

Another image can be supplied as an argement in convert, in which case conversion is to match that
image type.
