#python
#pygame

Block any mouse motion from triggering event:
pygame.event.set_blocked(MOUSEMOTION)

To unblock specific events, e.g. mouse motion:
pygame.event.set_allowed(MOUSEMOTION)

Block all keyboard events:
pygame.event.set_blocked([KEYDOWN, KEYUP])

To block all events:
pygame.event.set_allowed(None)

To unblock all events:
pygame.event_set_blocked(None)

To check whether an event type is blocked, e.g. mouse motion:
pygame.event.get_block(MOUSEMOTION)
