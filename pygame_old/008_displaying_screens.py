#python
#pygame

Create defined size display
---------------------------

screen = pygame.display.set_mode((640,480),0,32)

Coomon standard resolutions are:
640 x 480
800 * 600
1024 * 768

See see what modes a computer has use:

pygame.display.list_modes() after pygame.init()

Dell XPS-15 has:
(1920, 1080), (1680, 1050), (1600, 1024), (1440, 900), (1400, 1050),
(1360, 768), (1280, 1024), (1280, 960), (1152, 864), (1024, 768), 
(960, 720), (960, 600), (960, 540), (928, 696), (896, 672), (840, 525), 
(800, 600), (800, 512), (720, 450), (700, 525), (680, 384), (640, 512), 
(640, 480), (576, 432), (512, 384), (400, 300), (320, 240)]

Note: Pygame will aways try to find a screen and colour resulution that works if called-for 
resolution does not work.



Create full screen display
--------------------------
# Note: this needs a way of exiting the game as the close window button is not present

screen = pygame.display.set_mode((640,480),FULLSCREEN,32)


