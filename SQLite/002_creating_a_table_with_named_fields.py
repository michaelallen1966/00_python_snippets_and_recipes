#python
#sqlite

import sqlite3

con = sqlite3.connect('sgd.db') # opens or creates a database
cur = con.cursor() # set up cursor to send commands to

# Delete table features if it already exists
cur.execute('drop table if exists features')

features = 'id text,kind text,status text,orf text,name text,aliases text,parent text,sec_ids text,' \
'chromosome integer,start integer,finish integer,direction text,genetic_pos int,' \
'cordinat_ver text,sequence_vers text,decription text'
execute_txt = 'create table features (' + features + ')'

cur.execute(execute_txt)

con.commit() # commit changes
con.close() # close database
