def dfs_recursive(graph, vertex, path=[]):
    # Add starting node to path
    path += [vertex]

    # Search from current node
    for neighbor in graph[vertex]:
        if neighbor not in path:
            # Next node is added to path as part of the recursive function call
            # (See line 3)
            path = dfs_recursive(graph, neighbor, path)

    return path

# adjaceny matrix:
# Node 1 connects to nodes 2 & 3
# Node 2 connects to nodes 4 & 5
    # etc

adjacency_matrix = {1: [2, 3], 2: [4, 5],
                    3: [5], 4: [6], 5: [6],
                    6: [7], 7: []}

print(dfs_recursive(adjacency_matrix, 1))

