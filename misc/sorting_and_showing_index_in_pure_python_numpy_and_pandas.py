#python
#sort
#numpy
#pandas

# Using sort or sorted to sort an array
x=['c','a','b']
y=[2,5,3]
y.sort(reverse=True)
print(y)
z=sorted(x,reverse=True)
print(z)

# To show the index of a sorted array using python alone
x=['c','a','b']
sort_index = sorted(range(len(x)), key=lambda k: x[k],reverse=True)
print(sort_index)

# Using NumPy to show index of sorted array
import numpy as np
npx=np.array([10,2,6,7,4])
sorted_array=np.sort(npx)
sort_index = np.argsort(npx)
print(sorted_array)
print(sort_index)

# Using Pandas to show index of sorted series
import pandas as pd
#pdx=pd.Series([10,2,6,7,4])
pdx=pd.Series(['c','a','b'])
pdx.sort()
pdx_order=list(pdx.index)
pdx_values=list(pdx.values) # Will generate array without list (NumPy array for numbers)

