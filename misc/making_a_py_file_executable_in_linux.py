#python
#linux

create new file with:

touch hello_world.py

Make executable with:

chmod +x hello_world.py

Edit file with gedit hello_world.py

Use a shebang in front of programme to show it is python 3:

#!/usr/bin/env python3
print('Hello World')

Run with:

./hello_world.py

Or the shebang can be removed and use: 

python3 ./hello_world.py
