#python
#module
#package
#oop
#class


modules
=======

A module is a single file (or files) that are imported under one import and used. e.g

import my_module


packages
========

A package is a collection of modules in directories that give a package hierarchy.

from my_package import my_module

The package directory should contain a file (which is empty) called __init__.py

Usually the package directory is in the same location as the py file.
The modeules are within the package


importing class from module inside package
==========================================

from my_package.my_module import My_classt