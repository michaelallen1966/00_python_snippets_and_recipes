"""
Open development server at http://127.0.0.1:5000/

This code returns a 404 error if user not found
Code continues to run (so correct user may be entered)
"""


from flask import Flask
from flask import abort

my_users = {'1':'mike','2':'nicholas'}

# Set up instance of Flask object
app = Flask(__name__) # root path of Python code; __name__ is good default

# define route for incoming web traffic 
# can also use name/integer to custom-route, e.g. @app.route('user/<name>')

@app.route('/')
def index():
    return '<h1>Hello World</h1>'

@app.route('/user/<id>')
def user(id):
    try:
        name = my_users[id]
    except:
        abort(404)   
    return '<h1>Hello, %s!</h1>' % name

# Start up a development server if Python code run directly
if __name__ == '__main__':
    app.run(debug = True)

