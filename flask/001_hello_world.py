"""
Open development server at http://127.0.0.1:5000/
"""

from flask import Flask

# Set up instance of Flask object (all flask apps need this)
app = Flask(__name__) # root path of Python code; __name__ is usual default

# define route for incoming web traffic 
# can also use name/integer to custom-route, e.g. @app.route('user/<name>')

# The example below registers index() as the handler for the applications
# root url (/)

@app.route('/')
def index():
    return '<h1>Hello World</h1>'

# The example below returns a different response for 
# http://127.0.0.1:5000/michael

@app.route('/michael')
def index_michael():
    return '<h1>Hello Michael</h1>'

# Start up a development server if Python code run directly
if __name__ == '__main__':
    app.run(debug = True)

