#python
#numpy
#class
#oop

class Points :

    def __init__(self, x, y):
        self.x = np.asarray(x)
        self.y = np.asarray(y)

    def shift_left(self, distance):
        self.x -= distance

x = np.random.random(10000)
y = np.random.random(10000)

z=Points(x,y)

print(z.x)