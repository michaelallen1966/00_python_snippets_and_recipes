#python
#numpy
#concatenate

numpy.concatenate

Behaves very similar to stack (stack, hstack and vstack use concatenate)


numpy.concatenate((a1, a2, ...), axis=0)
Join a sequence of arrays along an existing axis.


Parameters:
a1, a2, ... : sequence of array_like

The arrays must have the same shape, except in the dimension corresponding to axis (the first, by default).
axis : int, optional

The axis along which the arrays will be joined. Default is 0.

Returns:
res : ndarray
The concatenated array.

Examples
>>>
>>> a = np.array([[1, 2], [3, 4]])
>>> b = np.array([[5, 6]])
>>> np.concatenate((a, b), axis=0)
array([[1, 2],
       [3, 4],
       [5, 6]])
>>> np.concatenate((a, b.T), axis=1)
array([[1, 2, 5],
       [3, 4, 6]])

This function will not preserve masking of MaskedArray inputs (for that use ma.concatenate)