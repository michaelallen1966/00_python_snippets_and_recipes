#python
#numpy
#mask
#boolean

import numpy as np
import pprint
import random

pp = pprint.pprint  # make pp

## String array
data4 = (['Bob', 'James', 'Bob', 'Danny', 'James', 'Bob', 'Danny'])
array4 = np.array(data4)

# set up random array
array5 = np.zeros((7, 4))
array5[:, :] = random.random()
for i in range(7):
    for j in range(4):
        array5[i, j] = random.random()

pp(array5)
print()

pp(array4 == "Bob")
print()
# Pass a boolean array to a number array
# The boolean array must be the same length as the array it is indexing


pp(array5[array4 == "Bob"])

# Or create a more complex mask
# note in boolean arrays '&' and '|' have to be used in place of 'and' and 'or')
mask = (array4 == "Bob") | (array4 == "James")  # | = or
print()
pp(array5[mask])

# Set all values less than 0.5 to zero


array5[array5 < 0.5] = 0
print()
pp(array5)

# Set all values where name is 'Bob' to 100

array5[array4 == "Bob"] = 100
print()
pp(array5)

==================================================================================

Two arrays may be examined simultaneously

x=np.array([0,1,1.5,0])
y=np.array([1,2,3,6])
c=(x>0) & (y<3)
print(c)

In [67]: c
Out[67]: array([False, True, False, False], dtype=bool)

