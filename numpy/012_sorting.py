#python
#numpy
#sort

import numpy as np 

# Simple sort with index

x=np.array([5.2,1,3,11,12,100,0])
sorted_x=np.sort(x)
sorted_x_index=np.argsort(x)
print('original vector, sorted vector, original index of sorted vector')
print (x)
print(sorted_x)
print(sorted_x_index)
print()

# Applying index to sort array by given column

x=np.array([[1.2,5.6],
            [10,0.3],
            [55,0],
            [2.2,7.7],
            [8,3]])
y=x[:,1] # second column of x used for sorting
sort_order=np.argsort(y)
print('Original matrix, matrix sorted by second column')
print(x)
print()
print(x[sort_order,:])

# Note order may be reverse using sort_order=sort_order[::-1]
