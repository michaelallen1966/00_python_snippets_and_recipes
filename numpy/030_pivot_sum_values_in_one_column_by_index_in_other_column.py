#python
#numpy
#bincount
#pivot

# Sum values in one column using value in another column as an index value

import numpy as np

# Build pretend array

array1=np.random.randint(10, size=(100,1))
array2=np.random.randint(1000, size=(100,1))
array3=np.append(array1,array2, axis=1) # building pretend array of patients and hospitals

# Sum by hospital

hospitals=array3[:,0] # need to extract data to a one dimensional array
patients=array3[:,1] # need to extract data to a one dimensional array
# Sum patients for each hospital
summed=np.bincount(hospitals,weights=patients)