#python
#numpy
#column_stack


Stack 1-D arrays as columns into a 2-D array.
Take a sequence of 1-D arrays and stack them as columns to make a single 2-D array. 
2-D arrays are stacked as-is, just like with hstack. 1-D arrays are turned into 2-D columns first.


>>> a = np.array((1,2,3))
>>> b = np.array((2,3,4))
>>> np.column_stack((a,b))
array([[1, 2],
       [2, 3],
       [3, 4]])