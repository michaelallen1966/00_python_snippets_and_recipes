#python
#numpy
#split


Split an array into multiple sub-arrays.


Parameters:
ary : ndarray
Array to be divided into sub-arrays.
indices_or_sections : int or 1-D array
If indices_or_sections is an integer, N, the array will be divided into N equal arrays along axis. 
If such a split is not possible, an error is raised.

If indices_or_sections is a 1-D array of sorted integers, the entries indicate where along axis the array is split. 
For example, [2, 3] would, for axis=0, result in

ary[:2]
ary[2:3]
ary[3:]

If an index exceeds the dimension of the array along axis, an empty sub-array is returned correspondingly.
axis : int, optional
The axis along which to split, default is 0.

Returns:sub-arrays : list of ndarray (a list of sub-arrays).

Examples
>>>
>>> x = np.arange(9.0)
>>> np.split(x, 3)
[array([ 0.,  1.,  2.]), array([ 3.,  4.,  5.]), array([ 6.,  7.,  8.])]
>>>
>>> x = np.arange(8.0)
>>> np.split(x, [3, 5, 6, 10])
[array([ 0.,  1.,  2.]),
 array([ 3.,  4.]),
 array([ 5.]),
 array([ 6.,  7.]),
 array([], dtype=float64)]