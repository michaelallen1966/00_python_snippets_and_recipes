#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check memomry id of object, to show whether two objects point to same
memory
"""

class MyFactory:
    pass

if __name__ == "__main__":
    a = MyFactory()
    b = MyFactory
    c = 10
    d = c # creates an alias to same memomry location
    print ('Memory loc of object a:', id(a))
    print ('Memory loc of object b:',id(b))
    print ('Memory loc of object c:',id(c))
    print ('Memory loc of object d:',id(d))
    
    # But manipluating d (the alias of c) will create a new object
    d *= 2
    print ('Memory loc of object c:',id(c))
    print ('Memory loc of object d:',id(d))
