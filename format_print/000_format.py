

# Strings

print_string = "Sammy loves {} {}."
print(print_string.format("open-source", "software")) 

print_string = "Sammy loves {0} {1}."
print(print_string.format("open-source", "software")) 

print_string = "Sammy loves {1} that is {0}."
print(print_string.format("open-source", "software")) 

# Number precision
print("Sammy ate {0:.2f} percent of a {1}!".format(75.12345, "pizza"))

# Padding
print('Helen has {0:2} balloons'.format(5))
print('Peter has {0:2} balloons'.format(15))

# Using variables
percent = 71.53456
food = 'pizza'
print("Sammy ate {0:.0f} percent of a {1}!".format(percent, food))

for i in range(3,13):
    print("{:3d} {:4d} {:5d}".format(i, i*i, i*i*i))
    
# Datetime
import datetime
# from datetime we can use date, time, or datetime

now = datetime.datetime.now()
print(now)
print(now.date())
print(now.time())
# Convert to string format
str = datetime.datetime.strftime(now, format='%A')
print(str)

"""Datetime formats:
    
    
    %a: Returns the first three characters of the weekday, e.g. Wed.
    %A: Returns the full name of the weekday, e.g. Wednesday.
    %B: Returns the full name of the month, e.g. September.
    %w: Returns the weekday as a number, from 0 to 6, with Sunday being 0.
    %m: Returns the month as a number, from 01 to 12.
    %p: Returns AM/PM for time.
    %y: Returns the year in two-digit format, that is, without the century. For example, "18" instead of "2018".
    %f: Returns microsecond from 000000 to 999999.
    %Z: Returns the timezone.
    %z: Returns UTC offset.
    %j: Returns the number of the day in the year, from 001 to 366.
    %W: Returns the week number of the year, from 00 to 53, with Monday being counted as the first day of the week.
    %U: Returns the week number of the year, from 00 to 53, with Sunday counted as the first day of each week.
    %c: Returns the local date and time version.
    %x: Returns the local version of date.
    %X: Returns the local version of time.
"""
