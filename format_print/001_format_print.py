#python
#print
#format
#decimal places

my_string='Michael'
print('Place my string here: %s' %(my_string))

Output: Place my string here: Michael

(Note %s will convert numbers to a string)

x=3.14159
print('Place my string here: %s' %(x))

Output: Place my string here: 3.14159

Or floating numbers may be formated with %x.yf where x is the minimum space to be used (adds white space to left of number as necessary) and y is the decimal places

print('Place my string here: %1.2f' %(x))

Output: Place my string here: 3.14

Multiple inserts

my_str1='Michael'
my_str2='Allen'
print('First name: %s, Second name %s' %(my_str1,my_str2))

Output: First name: Michael, Second name Allen

Or to print in any order:

my_str1='Michael'
my_str2='Allen'
print('Second name: {y}, First name {x}'.format(x=my_str1,y=my_str2))

Output: Second name: Allen, First name Michael
