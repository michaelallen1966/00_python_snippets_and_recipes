#python
#oop

 #import math

class Point:
    def reset(self):
        self.x=0
        self.y=0

    def move(self,x,y): # move point to x,y
        self.x=x
        self.y=y

    def calculate_distance(self,other_point):
        dist=math.sqrt((self.x-other_point.x)**2+(self.y-other_point.y)**2)
        return dist

p1=Point()
p2=Point()
p1.move(20,300)
p2.move(40,20)
print(p1.calculate_distance(p2)) # This passes the object calling and one other. It might also be done by Point.calculate_distance(p1,p2)

