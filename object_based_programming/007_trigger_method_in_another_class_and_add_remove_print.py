#python
#oop
#print

class Parent(object):
    """The parent"""
    def order_bed (self,child): # see comment below
        print("Go to bed",child.myname,"!!!")
        child.go_to_bed(child) # note this is child as defined in definition above
        bedroom1.addchild(child)

    def order_wake (self,child):
        print ("Wake up",child.myname,"!")
        child.wake_up(child)
        bedroom1.remove_child(child)
       

class Child(object):
    def __init__(self,name):
        self.myname=name
    def go_to_bed(self,child):
        print("Awwww. OK. Goodnight")
    def wake_up(self,child):
        print ("I want to carry on sleeping, but OK")
       

class Bedroom (object):
    def __init__(self):
        self.kids=[] # kids is a list of object members
    def __str__(self): # this method is called when print(object_name) is called
        if self.kids:
            sleeping=""
            for child in self.kids:
                sleeping+= child.myname+" "
        else:
            sleeping = "No-one"
        return sleeping
    
    def addchild(self,child):
        self.kids.append(child)
        print("The bedroom now holds")
        print(bedroom1)


    def remove_child(self,child):
        self.kids.remove(child)
        print("The bedroom now holds")
        print(bedroom1)