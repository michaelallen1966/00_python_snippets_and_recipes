#python
#class
#oop
#inheritance
#initiation

In the example below Pet is the base class and Dog is the derived class



class Pet(object):
    """A virtual pet"""
     
    def __init__(self): # this runs when a new instance is created
        print("A new pet has been born!")
        number_of_legs=0 #this sets a default value of 0 legs
    def count_legs(self):
        print("I have %s legs." % self.number_of_legs)

class Dog(Pet):
    """A virtual dog"""
    def __init__(self): # this runs when a new instance is created. It over-rides the Pet initiation method.
        print("A new dog has been born!")
        number_of_legs=4 #this sets a default value of 4 legs
    def bark(self):
        print("Woooffff!!")
   

doug = Pet()
doug.number_of_legs=3
doug.count_legs()

timmy=Dog()
timmy.count_legs()
timmy.bark()
