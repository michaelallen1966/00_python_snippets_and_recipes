#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
setattr is used to set arbitary attributes on an object
"""

class MyClass:
    def __init__(self, name, **kwargs):
        self.name = name
        
        # Set arbitary attributes
        for key in kwargs:
            setattr(self, key, kwargs[key])
        
my_object = MyClass('Michael', age=52, iq=500)

print (my_object.name)
print (my_object.age)
print (my_object.iq)
