x = [1, 2, 3, 4, 5, 6]

# drop first and last (use _ as a name for disposable items)
# use * to unpack iterable passed to it; this will be the middle portion of
# the string
_, *x, _ = x
print(x)

# *also works to unpack strings (as they are interable).
x = 'hello'
print (*x)

# Nested iterables may use *, *_ is used to throw away unwanted items
x = ['Bob', 2, 3, 4, ['Birmingham', 5, 6]]
name, *_, [town, *_] = x
print (name, town)

# Unpacking can be used to get head or tail of list (or other iterable)
x = ['Aadvark','Cat,','Dog','Elephant','Zebra']
head, *_ = x
*_, tail = x
print (head, tail)
