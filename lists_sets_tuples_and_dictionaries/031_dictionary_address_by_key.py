#python
#dictionary

Dictionaries are like lists, but use a key to find element.

# Assigning a dictionary with three key-value pairs to residents:
residents = {'Puffin' : 104, 'Sloth' : 105, 'Burmese Python' : 106}

print residents['Puffin'] # Prints Puffin's room number
print residents['Sloth']
print residents['Burmese Python']

Elements may be defined as follows

zoo_animals = { 'Unicorn' : 'Cotton Candy House',
'Sloth' : 'Rainforest Exhibit',
'Bengal Tiger' : 'Jungle House',
'Atlantic Puffin' : 'Arctic Exhibit',
'Rockhopper Penguin' : 'Arctic Exhibit'}


Elements are added as follows:

menu = {} # Empty dictionary
menu['Chicken Alfredo'] = 14.50 # Adding new key-value pair
print menu['Chicken Alfredo']
menu['Chicken Bungo'] = 12.60
menu['Veggie Madras'] = 10.00



print "There are " + str(len(menu)) + " items on the menu."
print menu

elements may be deleted

del menu[�Chiken Bungo�]

elements may be changed
menu['Veggie Madras'] = 12.00

Dictionaries can contain a variety of element types

my_dict = {   
     "fish": ["c", "a", "r", "p"],
    "cash": -4483,
    "luck": "good" }

print my_dict["fish"][0]

Adding and sorting a new list in a dictionary

inventory['pocket']=['seashell','strange berry','lint']
inventory['pocket'].sort()

removing an element

del inventory['backpack'][1]
or use name
inventory['backpack'].remove('dagger')

examples of functions


inventory = {
'gold' : 500,
'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
}

# Adding a key 'burlap bag' and assigning a list to it
inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

# Sorting the list found under the key 'pouch'
inventory['pouch'].sort()

inventory['pocket']=['seashell','strange berry','lint']
inventory['backpack'].sort()

# del inventory['backpack'][1]
inventory['backpack'].remove('dagger')
print inventory['backpack']
inventory['gold']+=50


Using list functions in a dictionary

dict_name['list_key'].list_function()
