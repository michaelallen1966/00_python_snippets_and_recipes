#python
#dictionary
#sort

from collections import OrderedDict

# OrderedDict maintains the original entry order of the dictionary
# Useful if it will help to iterate over the dictionary in the original order
# Ordered dictionaries can use 2x memory of ordinary dictionaries

d=OrderedDict()

d['foo']=1
d['bar']=2
d['spam']=3
d['grok']=4

for key in d:
    print(key,d[key])
