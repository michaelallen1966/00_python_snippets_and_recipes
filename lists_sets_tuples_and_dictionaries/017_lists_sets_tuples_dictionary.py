#python
#list
#tuple
#dictionary
#set



Lists - any items, duplicates allowed []
Set - duplicates not allowed {}
Tuple - cannot be changed (can be added to and elements deleted) ()
Dictionaries - use key rather than position {}

Use dictionary for fast lookup of random access
Use list for iterable
Use sets for unique values
Use tuples for immutable lists
