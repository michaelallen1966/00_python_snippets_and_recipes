#Initialisie dictionary
d={}

# Add dictionary entries
d['Gandalf'] = 10
d['Bilbo'] = 4
d['Frodo'] = 5
d['Sam'] = 2

# To find who has the highest power we invert keys and values using zip
print ('Greatest power')
print (max(zip(d.values(),d.keys())))

# And the same for the minumum
print ()
print ('Least power')
print (min(zip(d.values(),d.keys())))

# Ranking values
power_sorted = sorted(zip(d.values(),d.keys()))
print ()
print ('Sorted by power')
print (power_sorted)
