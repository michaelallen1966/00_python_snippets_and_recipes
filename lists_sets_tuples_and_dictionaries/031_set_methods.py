#python
#set


Set Methods

A set object has a number of member methods. In the following definitions, s is a set object.

The following transformation functions update a set.

s. clearRemove all items from the set.

s. copy ? setCopy the set to make a new set. This is a shallow copy. 
All objects in the new set are references to the same objects as the original set.

s. pop ? objectRemove an arbitrary object from the set, returning the object. 
If the set was already empty, this will raise a KeyError exception.

s. add (new )Adds element new to the set. If the object is already in the set, nothing happens.

s. remove (old )Removes element old from the set. If the object old is not in the set, 
this will raise a KeyError exception.

s. discard (old )Removes element old from the set. If the object old is not in the set, nothing happens.

s. update (new ) ? objectMerge values from the new set into the original set, adding elements as needed.
It is equivalent to the following Python statement s |= new.

s. intersection_update (new ) ? objectUpdate s to have the intersection of s and new . 
In effect, this discards elements from s , keeping only elements which are common to new and s . 
It is equivalent to the following Python statement. s &= new.

s. difference_update (new ) ? objectUpdate s to have the difference between s and new . 
In effect, this discards elements from s which are also in new . 
It is equivalent to the following Python statement. s -= new.

s. symmetric_difference_update (new ) ? objectUpdate s to have the symmetric difference between s and new .
 In effect, this both discards elements from s which are common with new and also inserts elements into s 
which are unique to new . It is equivalent to the following Python statement. s ^= new.


The following accessor methods provide information about a set.

s. issubset (set ) ? booleanIf s is a subset of set , return True, otherwise return False. 
Essentially, this is s <= set.

s. issuperset (set ) ? booleanIf s is a superset of set , return True, otherwise return False. 
Essentially, this is s >= set.

s. union (new ) ? setIf new is a proper set, return s | new. If new is a sequence or other iterable, 
make a new set from the value of new , then return the union, s | new. This does not update s .
>>> prime.union( (1, 2, 3, 4, 5) )set([1, 2, 3, 4, 5, 7, 11, 13])

s. intersection (new ) ? setIf new is a proper set, return s & new. 
If new is a sequence or other iterable, make a new set from the value of new , then return the intersection, s & new. 
This does not update s .

s. difference (new ) ? setIf new is a proper set, return s - new. 
If new is a sequence or other iterable, make a new set from the value of new , then return the difference, s - new. 
This does not update s .

s. symmetric_difference (new ) ? setIf new is a proper set, return s ^ new. 
If new is a sequence or other iterable, make a new set from the value of new, then return the symmetric difference, s ^ new. 
This does not update s .
