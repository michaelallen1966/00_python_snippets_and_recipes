a = {
    'x' : 1,
    'y' : 2,
    'z' : 3
}

b = {
    'w' : 10,
    'x' : 11,
    'y' : 2
}

# Dictionay keys and items can be handled like sets
# Values cannot be because they are not necessarily unique
# Examples

# Find keys in common
print (a.keys() & b.keys())

# Keys not in common
print (a.keys() - b.keys())

# Key, value pairs in common
print (a.items() & b.items())



