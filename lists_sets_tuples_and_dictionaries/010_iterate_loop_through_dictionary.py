#python
#dictionary

webster = {
     "Aardvark" : "A star of a popular children's cartoon show.",
    "Baa" : "The sound a goat makes.",
    "Carpet": "Goes on the floor.",
    "Dab": "A small amount."
}

# Method 1

for key in webster:
    print (key,' - ', webster[key])
    
print('\n')

# Method 2

for key,value in webster.items():    
    print ("%s - %s" % (str(key), str(value)) )   
    