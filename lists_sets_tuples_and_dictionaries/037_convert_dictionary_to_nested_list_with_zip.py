#python
#dictionary
#list
#zip

# note: a zipped object empties as values are used

import pprint # pretty print
pp=pprint.pprint # make pp shorthand for pprint.pprint

webster = {
     "Aardvark" : "A star of a popular children's cartoon show.",
    "Baa" : "The sound a goat makes.",
    "Carpet": "Goes on the floor.",
    "Dab": "A small amount."
}

    
x=list(webster.keys())    
y=list(webster.values())    
zipped = zip(x, y)
z=list(zipped)
pp(z)
print('\n')


# or use zip object directly
# note that that the zipped object is generated again because is is emptied above
# when a list is generated from it

zipped = zip(x, y)
for a,b in zipped:
    print(a,': ',b)
