my_str_1 = "here is my first string"
my_str_1 = str.capitalize(my_str_1)
print (my_str_1)

# As Python knows that my_str is a string the method may be called directly...

my_str_2 = "here is my second string"
my_str_2 = my_str_2.capitalize()
print (my_str_2)
