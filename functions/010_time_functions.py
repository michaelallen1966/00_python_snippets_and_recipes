import time

def my_loop(n):
    for i in range(n):
        _x = i**0.5
    return()
    
# Using time

t1  = time.perf_counter()
my_loop(10000)
t2  = time.perf_counter()
print ("{0:.3}".format((t2-t1)*1000)) # convert to ms

