#python

def sums(high,low):
    middle=(high+low)/2
    multiple=high*low
    return middle,multiple

result=(0,0)
lower=float(input("Think of a low number... "))
higher=float(input("Think of a low number... "))
result=sums(higher,lower)
print("The average is",result[0])
print("The product is",result[1])


Note: Position of arguments is critical, but this can be avoided if the name used in the function header is used in calling the function.....

	results=sums(high=higher,low=lower)

will return the same as 

	results=sums(low=lower,high=higher)

Default values may be set in the function (default values are used unless over-ridden by passed parameters), such as ....

	def sums(high=10,low=0):

Once a default is set all parameters to the right of the default must be set with a default

Putting the main procedure in a function encapsulates its variables.
