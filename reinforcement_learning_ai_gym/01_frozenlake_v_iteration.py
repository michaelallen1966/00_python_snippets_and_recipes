#!/usr/bin/env python3


"""
Value iteration method - learns values of states.
Chooses action based on target state distributions/value of different actions 

Value iteration method updates values when tansition probabilities and 
rewards are known*. This method allows future rewards to be taken into account,
but requires knowledge off possible actions and probabilities to be known, and 
can run into problems that have very large scale.

*transition and rewards may be learned from experience

General method:
    1) Play 100 steps, populating thr reward and transitions table
    2) Value iteration loop to update all state values
    3) Play several full episodes to check performance (and keep updating 
       reward + transition tables)
    4) If performance < taget 0.8 boundary then repeat from (1)
"""

# To reset tensorboard in bash: fuser 6006/tcp -k



import gym
import collections

ENV_NAME = "FrozenLake8x8-v0" # FrozenLake-v0 or FrozenLake8x8-v0
GAMMA = 0.9 # Discount rate
TEST_EPISODES = 20


class Agent:
    def __init__(self):
        self.env = gym.make(ENV_NAME)
        # For frozen lake print (self.env.render()) to show lake
        self.state = self.env.reset()
        # Rewards dictionary: key = source state + action + target state
        # This dictionary type will create a new entry if incrementation fails
        self.rewards = collections.defaultdict(float)
        # Transitions table: key = source state + action, value = dictionary of
        # target states and the number of times it has occured
        self.transits = collections.defaultdict(collections.Counter)
        # Value table: dictionary of state and value
        self.values = collections.defaultdict(float)

    def play_n_random_steps(self, count):
        # Gather rwweards and transitions from random exploration
        # Complete episodes are not needed
        # Loop through x random steps (e.g. 100)
        for _ in range(count):
            # Chose action from random sampling
            action = self.env.action_space.sample()
            # Get transition and reward (and episode complete)
            new_state, reward, is_done, _ = self.env.step(action)
            # Record reward and transition
            self.rewards[(self.state, action, new_state)] = reward
            self.transits[(self.state, action)][new_state] += 1
            # Set new state as current state (or reset if episode complete)
            self.state = self.env.reset() if is_done else new_state

    def calc_action_value(self, state, action):
        # Calculate value of state-action from transition, reward and value tables
        target_counts = self.transits[(state, action)] # transitions
        total = sum(target_counts.values())
        action_value = 0.0
        for tgt_state, count in target_counts.items():
            # Sum value by summed Bellman equation, weighted by transition counts
            reward = self.rewards[(state, action, tgt_state)]
            action_value += (count / total) * (reward + GAMMA * self.values[tgt_state])
        return action_value

    def select_action(self, state):
        # Select action uses the calc_action_value and selects highest value
        # Note that this action is deterministic (we assume the random exploration
        # provides sufficient exploration)
        best_action, best_value = None, None
        for action in range(self.env.action_space.n):
            action_value = self.calc_action_value(state, action)
            if best_value is None or best_value < action_value:
                best_value = action_value
                best_action = action
        return best_action

    def play_episode(self, env):
        # Play one full episode (uses a new environemnt to avoid resetting the 
        # random exploration environment
        total_reward = 0.0
        state = env.reset()
        while True:
            action = self.select_action(state)
            new_state, reward, is_done, _ = env.step(action)
            # Update reward and transition tables
            self.rewards[(state, action, new_state)] = reward
            self.transits[(state, action)][new_state] += 1
            total_reward += reward
            if is_done:
                break
            state = new_state
        return total_reward

    def value_iteration(self):
        # Iterate over all states and update values
        for state in range(self.env.observation_space.n):
            # Find maximum action value (becomes the new state value)
            state_values = [self.calc_action_value(state, action)
                            for action in range(self.env.action_space.n)]
            self.values[state] = max(state_values)


if __name__ == "__main__":
    test_env = gym.make(ENV_NAME)
    agent = Agent()
    iter_no = 0
    best_reward = 0.0
    while True:
        iter_no += 1
        agent.play_n_random_steps(1000)
        # I put this loop in to allow better feedback of value
        for j in range(100):
            agent.value_iteration()

        reward = 0.0
        for _ in range(TEST_EPISODES):
            reward += agent.play_episode(test_env)
        reward /= TEST_EPISODES
        if reward > best_reward:
            print("Best reward updated %.3f -> %.3f" % (best_reward, reward))
            best_reward = reward
        if reward > 0.80:
            print("Solved in %d iterations!" % iter_no)
            break

