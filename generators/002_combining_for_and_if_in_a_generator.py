#python
#generator

# Generator created using list comprehension but with () in place of []
gen = (x for x in range (100) if x%2==0)

# Show that my_gen is a generator
print (type(gen))

for x in gen:
     print (x)
