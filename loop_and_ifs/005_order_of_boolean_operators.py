#python

When no parentheses:

1. not is evaluated first;
2. and is evaluated next;
3. or is evaluated last.


Or use parentheses to control order
