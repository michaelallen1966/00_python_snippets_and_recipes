#python
#pandas
#statistics
#descrive
#groupby
#unstack

import pandas as pd
data_df=pd.read_csv('example1.csv')
search=pd.DataFrame.duplicated(data_df) # To identify duplicates with no action
filtered=data_df.drop_duplicates() # to remoe duplicates
summary=filtered.describe() # statistical summary
grouped=filtered.groupby('Type').describe() # Stratify summary by 'type' heaing
unstacked=grouped.unstack() #Unstack summary
print(unstacked.loc[:,(slice(None),['count','mean']),]) # Print only count and mean
