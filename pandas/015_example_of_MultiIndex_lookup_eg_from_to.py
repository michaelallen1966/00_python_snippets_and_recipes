#python
#pandas
#MultiIndex


import numpy as np
import pandas as pd
# Create random arrays for distance (100 rows of 20 columns),
# and lists of 10 random start and end points"""
dist_matrix=np.random.uniform(1,100,[100,20]) #  uniform random array size 100 rows of 20 cols
from_list=np.random.randint(0,100,[10])
to_list=np.random.randint(0,20,[10])
# Create a Pandas series with a multilevel (tuple) index
# Index is tuple of start and end point
# Series value is distance
index_tuple_list=[]
distance_list=[]
for row in range(len(dist_matrix[:,0])):
    for col in range(len(dist_matrix[0,:])):
        ind_tuple=(row,col)
        index_tuple_list.append(ind_tuple)
        distance=dist_matrix[row,col]
        distance_list.append(distance)
multi_index = pd.MultiIndex.from_tuples(index_tuple_list, names=['from', 'to'])
ds_distance=pd.Series(distance_list,index=multi_index,name='Distance')
# Create test list tuple index
test_list=[]
from_to=[]
for from_id,to_id in zip(from_list,to_list):
    from_to=(from_id,to_id)
    test_list.append(from_to)
# Look up distances   
test_distances=ds_distance.ix[test_list] # Panda series
print(test_distances)
test_distance_values=np.array(test_distances) # Convert to NumPy array
print(test_distance_values)
