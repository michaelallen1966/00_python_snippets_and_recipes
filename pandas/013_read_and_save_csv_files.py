#python
#pandas
#csv
#file


For more file loading see: http://pandas.pydata.org/pandas-docs/stable/io.html


import pandas as pd

# The following imports a CSV from the same folder as the program code. This imports numbers and text into a Panda dataframe. This imports first row as title
patient_nodes_panda=pd.read_csv('node_admissions_with_postcodes.csv')

# The following imports a TXT from the same folder as the program code. This imports numbers and text into a Panda dataframe. This imports first row as title
patient_nodes_panda=pd.io.parsers.read_table('node_admissions_with_postcodes.txt)
# or
patient_nodes_panda=pd.read_table('node_admissions_with_postcodes.txt)


df.to_csv('filename.csv') to save dataframe as csv

