#python
#pandas
#index


import pandas as pd

songs_66 = pd.Series([3,None,11,9],
                     index=['George','Ringo','John','Paul'],
                     name='Counts') 

songs_69 = pd.Series([18,22,7,5],
                     index=['John','Paul','George','Ringo'],
                     name='Counts')





# retrieving index positions
george=pd.Series([10,7,1,22],
                 index=['1968','1969','1970','1970'],
                 name='George songs')
index_pos=[pos for pos,x in enumerate(george.iteritems()) if x[0]=='1970']
print('\nDisplay index positions: ',index_pos)

# Reset index
# .reset_index returns a dataframe (not series) by default, creating an ordinal index and moving current index to a new column
df=songs_66.reset_index()
print('\nReset_index to df')
print(df)

# To get a series out pass argument drop=True, note original index is lost
new_series=songs_66.reset_index(drop=True)
print('\nReset_index to new Series')
print(new_series)

# New index names may be passed. Values are carried forward only when old and new index names match
new_index=[['Billy','Eric','George','Yoko']]
new_series=songs_66.reindex(new_index)
print('\nReset_index to new Series with new given index names')
print(new_series)

# Renaming an index name
renamed_index=songs_66.rename({'Ringo':'Richard'})
print('\nRenamed index')
print(renamed_index)

# Changing the index 'manually' (not recommended as it treats a series as mutable)
idx=songs_66.index # keep originl index
idx2=range(len(idx))
songs_66.index=idx2
print()
print('Mechanically imputed index')
print(songs_66)