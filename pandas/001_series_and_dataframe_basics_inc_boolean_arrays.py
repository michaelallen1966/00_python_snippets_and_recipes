#python
#pandas
#iteritems

# see http://pandas.pydata.org/pandas-docs/stable/basics.html


import pandas as pd

songs2=pd.Series([145,142,38,13],name='counts')

# show index
print (songs2,songs2.index)

# Use string indrx
songs3=pd.Series([145,142,38,13],name='counts',index=['Paul','John','George','Ringo'])
print('\n',songs3,songs3.index)

# Note: series may be a mix of types (including objects)

# If dtype of series is object when it should be a number then it is possible some values have come in as strings (e.g. date text)

# When series is number and elements ar emissing or cannot be stored as number thwn NaN is used.
# NaN is ignored in numerical methods (including count)
# NaN forces series into float64 (cannot be int64)

# Index by number or index key
print()
print(songs3['John'])
print(songs3[1])
print(songs3.iloc[1]) # better way to address index location

# Simple mathematical operation
print()
print('mean',songs3.mean())

# Boolean array
mask = songs3 > songs3.median()
print()
print(mask)
print(songs3[mask])

import pandas as pd

george_dup=pd.Series([10,7,1,22],
                     index=['1968','1969','1970','1970'],
                     name='George songs')

# Note: index in above are strings and are duplicated

print(george_dup)

# When an index is repeated index references will return all rows as a series:
print(george_dup['1970'])

# Iterate over series:
for item in george_dup:
    print(item)

# To check for membership 'in' only looks at index.
# use 'set' to check in values
print()   
print('1970' in george_dup)
print(22 in set(george_dup))


# use 'iteritems' to check in both index and values - creates tuple for each entry
print()
for item in george_dup.iteritems():
    print(item)

# Adding a new entry item
george_dup['1973']=11
print()
print(george_dup)
# When an index value already exists (e.g. 1970), using this method will overwrite all previous values with new value
# Append method creates a new series (name of orginal series not carried forward!)
# Set_value may also be used to change or add to existing series

# To change a specifc value based on location used iloc method
print()
print(george_dup)
george_dup.iloc[1]=10
print(george_dup)

# Delete by index (deletion not commonly used in pandas)
del george_dup['1973']
print()
print(george_dup)

# Filter by index
print(george_dup[george_dup<=2])



=========================================================================================================
Dataframe basics

import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import pprint


pp=pprint.pprint # make pp


data = {'state': [' Ohio', 'Ohio', 'Ohio', 'Nevada', 'Nevada'], 'year': [2000, 2001, 2002, 2001, 2002], 'pop': [1.5, 1.7, 3.6, 2.4, 2.9]}


frame=DataFrame(data)
print(frame)
print()


#Columns may be ordered (any column heading without matching data will have missing data)
frame2=DataFrame( data, columns =['year', 'state', 'pop','debt'])
print(frame2)
print()


#Index values may also be passed
frame3=DataFrame( data, columns =['year', 'state', 'pop','debt'],index=['one','two','three','four','five'])
print(frame3)
print()


#Retrieving columns
print(frame2['state'])
print()
#Alternative syntax
print(frame2.state)
print()


#retrieving rows (better to use loc/iloc)
print (frame3.ix['three']) #access by index name
print()
print (frame3.ix[2]) # access directly
print()


#Adding and deletng colums
frame2['eastern'] = frame2. state == 'Ohio'
print(frame2)
print()
del frame2['eastern']
print(frame2)
print()

#Addign a scaler value
frame2['Addedscalar']=23.3
print(frame2)
print()


#Tansposing
frame4=frame2.T
print(frame4)
print()


#Returning values (without index) to NumPy ndarray
array=frame4.values
pp(array)
print()


Convert data frame to series (1D)
---------------------------------

SeriesName=DataFrameName["Column Name"]








