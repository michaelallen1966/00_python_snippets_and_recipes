#python
#pandas
#slice

see http://pandas.pydata.org/pandas-docs/stable/indexing.html


import pandas as pd

george=pd.Series([10,7],
                 index=['1968','1969'],
                  name='George songs')

dupe=pd.Series([10,2,7,1],
               index=['1968','1968','1969','1970'],
               name='George songs')

# slices take the form [start:end:stride]
print('\nSlices')
print('first two: ',dupe.iloc[0:2])
print('every other: ',dupe.iloc[0::2])

# boolean masks
mask=dupe>6
print('\n >6: ',mask,dupe[mask])
mask2=dupe<=1
print('\n >6 or =<1: ',dupe[(mask | mask2)]) # | is or

# Performing mathematical functions on series
# e.g. print(dupe,dupe+2)
