#python
#pandas
#pickle

import pandas as pd
import numpy as np

lsoa_to_postcode = pd.read_csv('lsoa_to_postcode.csv')
lsoa_to_postcode.to_pickle('pickles/lsoa_to_postcode')

# Read distance matrix which has index column named LSOA
travel_matrix_miles = pd.read_csv('full_travel_matrix_miles.csv', low_memory=False)
travel_matrix_miles = travel_matrix_miles.rename(columns={'\ufeffLSOA': 'LSOA'})
travel_matrix_miles.set_index('LSOA', inplace=True)
travel_matrix_miles = travel_matrix_miles.apply(pd.to_numeric, errors='coerce')
travel_matrix_miles.to_pickle('pickles/travel_matrix_miles')

travel_matrix_days = pd.read_csv('full_travel_matrix_days.csv', low_memory=False)
travel_matrix_days = travel_matrix_days.rename(columns={'\ufeffLSOA': 'LSOA'})
travel_matrix_days.set_index('LSOA', inplace=True)
travel_matrix_days = travel_matrix_days.apply(pd.to_numeric, errors='coerce')
travel_matrix_days.to_pickle('pickles/travel_matrix_days')
travel_matrix_minutes = travel_matrix_days * 1440
travel_matrix_minutes.to_pickle('pickles/travel_matrix_minutes')