#python
#pandas
#statistics

import pandas as pd

songs_66=pd.Series([3,None,11,9],
                   index=['George','Ringo','John','Paul'],
                   name='Counts')

scores2=pd.Series([67.3,100,96.7,None,100],
                   index=['Ringo','George','Paul','Peter','Billy'],
                   name='test2')

scores3=pd.Series([67.3,100,96.7,None,100,79],
                   index=['Ringo','Paul','George','Peter','Billy','Paul'],
                   name='test3')

#  mean, median
print('mean: ',songs_66.mean())
print('median: ',songs_66.median())

# quantiles (percentiles) need nan removed
clean=songs_66.dropna()
print('0.2 quantile: ',clean.quantile(0.2))

# General description
print('\nDescriptive stats without nan removal')
print(songs_66.describe())
print('\nDescriptive stats after nan removal')
print(songs_66.dropna().describe())
# Add in specific percentiles
print(songs_66.dropna().describe(percentiles=[0.05,0.1,0.9,0.95]))

# Get minimum and maximum with index location
print('\nMinimum with index')
print(songs_66.min(),songs_66.idxmin())
print('\nMaximum with index')
print(songs_66.max(),songs_66.idxmax())

"""Other statistical functions:
.var() = variance
.std() = standard deviation
.mad() = mean absolute variation
.skew() = skewness of distribution
.kurt() = kurtosis
.cov() = covariance
.corr() = Pearson Correlation coefficent
.autocorr() = autocorelation
.diff() = first discrete difference
.cumsum() = cummulative sum
.comprod() = cumulative product
.cummin() = cumulative minimum
