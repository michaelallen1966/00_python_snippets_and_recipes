"""
Tensoflow variables and calclulations are automatically added to the default
graph. But multiple graphs may be used by changing the default graph.
"""

import tensorflow as tf

tf_graph_1 = tf.Graph()
with tf_graph_1.as_default():
    x1 = tf.Variable(1)
    x2 = tf.Variable(1)
    
tf_graph_2 = tf.Graph()
with tf_graph_2.as_default():
    x3 = tf.Variable(10)

print ('Note x3 belongs to a different graph object from x1 and x2')
print (x1.graph)
print (x2.graph)
print (x3.graph)
    