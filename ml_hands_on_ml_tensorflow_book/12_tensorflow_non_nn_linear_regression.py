"""Linear regression. Not a neural net"""

import tensorflow as tf
import numpy as np
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import StandardScaler

# to make this code's output stable across runs
def reset_graph(seed=42):
    tf.reset_default_graph()
    tf.set_random_seed(seed)
    np.random.seed(seed)

# Get California housing data 
housing = fetch_california_housing()

# Create scaled data set
scaler = StandardScaler()
scaled_housing_data = scaler.fit_transform(housing.data)

# Add a bias column, X0, (allows for 'x' in y=mx+c)
m, n = housing.data.shape
scaled_housing_data_plus_bias = np.c_[np.ones((m, 1)), scaled_housing_data]

# Set up Gradient Descent TensorFlow

reset_graph()

n_epochs = 1000
learning_rate = 0.01

X = tf.constant(scaled_housing_data_plus_bias, dtype=tf.float32, name="X")
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name="y")
theta = tf.Variable(
        tf.random_uniform([n + 1, 1], -1.0, 1.0, seed=42), name="theta")
y_pred = tf.matmul(X, theta, name="predictions")
error = y_pred - y
mse = tf.reduce_mean(tf.square(error), name="mse")

# Set up optimizer

#optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

# Alternative optimizer (often faster)
optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate,
                                       momentum=0.9)

training_op = optimizer.minimize(mse)

# Run tensorflow, and return mean squared error

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)

    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print("Epoch", epoch, "MSE =", mse.eval())
        sess.run(training_op)
    
    best_theta = theta.eval()

print("Best theta:")
print(best_theta)