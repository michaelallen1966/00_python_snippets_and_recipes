# Note: Regularisation depends on feature scaling

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

################################################################################
######################  Plot learning curvesfunction ###########################
################################################################################
    
def plot_learning_curves(train_errors, val_errors, model_title):
    plt.plot(np.sqrt(train_errors), "r-+", linewidth=2, label="train")
    plt.plot(np.sqrt(val_errors), "b-", linewidth=3, label="val")
    plt.legend(loc="upper right", fontsize=14)
    plt.xlabel("Training set size", fontsize=14)
    plt.ylabel("RMSE", fontsize=14)
    plt.ylim(0,5)
    plt.title(model_title)
    plt.show() 
    return

################################################################################
################ Create  and plot linear data with noise #######################
################################################################################

m = 100 # number of samples

X = 6 * np.random.rand(m, 1) - 3
y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)

################################################################################
############### Use sklearn to create polynomia; features ######################
################################################################################

from sklearn.preprocessing import PolynomialFeatures
poly_features = PolynomialFeatures(degree=10, include_bias=False)
X_poly = poly_features.fit_transform(X)
print ('Original X[0]:', X[0])
print ('Polynomial X[0]:', X_poly[0])

################################################################################
############################## Standardise data ################################
################################################################################

################################################################################
#################  Split into training and test data sets ######################
################################################################################

X_train, X_val, y_train, y_val = train_test_split(
        X_poly, y, test_size=0.2, random_state=1)

################################################################################
################################  Standardise dta #############################
###############################################################################

# Standardise data based on training set
sc=StandardScaler() 
sc.fit(X_train)
X_train_std=sc.transform(X_train)
X_val_std=sc.transform(X_val)


def fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title):
    train_errors, val_errors = [], []
    for m in range(1, len(X_train_std)): # Use successively larger par of training set
        model.fit(X_train_std[:m], y_train[:m]), model_title
        y_train_predict = model.predict(X_train_std[:m])
        y_val_predict = model.predict(X_val_std)
        train_errors.append(mean_squared_error(y_train[:m], y_train_predict))
        val_errors.append(mean_squared_error(y_val, y_val_predict))
    plot_learning_curves(train_errors, val_errors, model_title)
    return


################################################################################
##########################  Un-regularised regression ##########################
################################################################################

model = LinearRegression()
model_title = 'Unregularised'
fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title)

################################################################################
############################  Ridge regularistion ##############################
################################################################################

# alpha is degree of regularisation and should be 
# Ridge regression tends towards reducing weight of all coefficients
# Ridge is best when all features have an effect

from sklearn.linear_model import Ridge
model = Ridge(alpha=1, solver="cholesky", random_state=42)
model_title = 'Ridge'
fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title)


################################################################################
############################  Lsoo regularistion ###############################
################################################################################

# Alpha is degree of regularisation and should be optimised
# Laso tends to eliminate weights of least important features

from sklearn.linear_model import Lasso
model = Lasso(alpha=0.1, random_state=42)
model_title = 'Lasso'
fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title)


################################################################################
########################  Elastic net regularistion ############################
################################################################################

# Alpha is degree of regularisation and should be optimised
# l1_ratio may also be optimised
# Elastic net is compromise between Ridge and Lasso

from sklearn.linear_model import ElasticNet
model = ElasticNet(alpha=0.1, l1_ratio=0.5, random_state=42)
model_title = 'Elastic net'
fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title)

################################################################################
#############  Stochastic Gradient descent with l2 regularisation ##############
################################################################################

from sklearn.linear_model import SGDRegressor
model = SGDRegressor(max_iter=50, tol=-np.infty, penalty="l2", random_state=42)
model_title = 'Stochastic Gradient descent\nwith l2 regularisation'
fit_model(model, X_train_std, X_val_std, y_train, y_val, model_title)



################################################################################
############################  Limiting epochs for SGD ##########################
################################################################################








