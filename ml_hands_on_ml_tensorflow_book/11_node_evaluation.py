"""
When TensforFlow evaluates a node, it will automatically determine the set of
nodes that it depends on and evalaute those first.
"""

import tensorflow as tf

# Define a simple graph
w = tf.constant(3)
x = w + 2
y = x + 5

# Initialise graph and evaluate y
with tf.Session() as sess:
    print (y.eval())
    
print()